-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 16, 2020 alle 15:59
-- Versione del server: 10.4.8-MariaDB
-- Versione PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibizabynight`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `PK_utente` int(10) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Cognome` varchar(20) NOT NULL,
  `Username` varchar(12) NOT NULL,
  `Password` varchar(128) NOT NULL,
  `Email` varchar(50) NOT NULL DEFAULT 'tuaemail@mail.com',
  `BigliettoAcquistato` int(11) NOT NULL,
  `Salt` varchar(128) NOT NULL,
  `Immagine` varchar(100) NOT NULL,
  `CodiceSicurezza` varchar(10) NOT NULL,
  `list_comment` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`PK_utente`, `Nome`, `Cognome`, `Username`, `Password`, `Email`, `BigliettoAcquistato`, `Salt`, `Immagine`, `CodiceSicurezza`, `list_comment`) VALUES
(88, 'Enrico', 'Roncuzzi', 'enri', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'prova@gmail.com', 0, 'c5c1204a1e43938159f4ca51cc799d12c6b544a65cace26aaf1036f1ae6fc1fe67aad5a24fcad243591dca24b4d27ae591ba1acad82755bca3c29097741e4bbe', 'DefaultProfilo.jpg', 'TadVHkmmkL', '164/163/162/161/160/159/'),
(89, 'Enrico', 'Roncuzzi', 'fabio', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'prova@gmail.com', 0, 'fe9ca3d8357203ffab8295b00402b53f94a4085d0cd43ae205529c1b69aded5018394873037cd71c20642e9f60e2d48edb650c57f4c7015bd1ccb0c935927329', 'DefaultProfilo.jpg', 'BiBHC8hLat', '');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`PK_utente`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `PK_utente` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

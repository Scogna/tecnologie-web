-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 13, 2020 alle 16:09
-- Versione del server: 10.4.8-MariaDB
-- Versione PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibizabynight`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `eventi`
--

CREATE TABLE `eventi` (
  `PK_evento` int(11) NOT NULL,
  `Nome` varchar(30) NOT NULL,
  `Descrizione` text NOT NULL,
  `Prezzo` int(11) NOT NULL,
  `Partecipanti` int(11) NOT NULL,
  `Ospite` varchar(30) NOT NULL,
  `Data` date NOT NULL,
  `Club` varchar(30) NOT NULL,
  `Immagine` varchar(100) NOT NULL,
  `InseritoDa` varchar(30) NOT NULL,
  `Approvato` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `eventi`
--

INSERT INTO `eventi` (`PK_evento`, `Nome`, `Descrizione`, `Prezzo`, `Partecipanti`, `Ospite`, `Data`, `Club`, `Immagine`, `InseritoDa`, `Approvato`) VALUES
(34, 'ElRow', 'hhhh', 5, 548, 'Alicante', '2020-12-24', 'DC10', 'defaultEvent.jpg', 'DC10', 1),
(36, 'CobraParty', 'asfafsfda', 70, 20, 'Fabio ', '2019-10-15', 'Amnesia', 'defaultEvent.jpg', 'DC10', 1),
(65, 'Pizzata', 'dsafdsa', 70, 2, 'Fabio ', '2020-01-08', 'Pacha', 'polygon-baboon-28703-2560x1600.jpg', 'DC10', 1),
(67, 'festa bellissima di bianca', ' fabio è invitato', 50, 2000, 'Ciani', '2020-03-15', 'DC10', 'images.jpg', 'DC10', 1),
(68, 'Compleanno Lucia', 'LA LUCIA SBRONZA', 10000, 2000, 'Lucia', '2020-02-16', 'BoatParty', 'defaultEvent.jpg', 'DC10', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `eventi`
--
ALTER TABLE `eventi`
  ADD PRIMARY KEY (`PK_evento`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `eventi`
--
ALTER TABLE `eventi`
  MODIFY `PK_evento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

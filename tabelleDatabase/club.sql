-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 13, 2020 alle 16:08
-- Versione del server: 10.4.8-MariaDB
-- Versione PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibizabynight`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `club`
--

CREATE TABLE `club` (
  `PK_club` int(11) NOT NULL,
  `Nome` varchar(15) NOT NULL,
  `Descrizione` text NOT NULL,
  `FamosaPer` text NOT NULL,
  `FesteFamose` text NOT NULL,
  `ComeArrivare` text NOT NULL,
  `DescrDett` text NOT NULL,
  `Immagine` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `club`
--

INSERT INTO `club` (`PK_club`, `Nome`, `Descrizione`, `FamosaPer`, `FesteFamose`, `ComeArrivare`, `DescrDett`, `Immagine`) VALUES
(1, 'DC10', 'Per essere famoso è famoso, quanto tutto ciò che c\' è sull\'isola, ma il DC10 ha raggiunto la posizione che ricopre grazie all\' esperienza unica che offre tra le discoteche di Ibiza. Quando fu inaugurato nel 1999, era un luogo di festa senza regole dove ballare fino ad essere sfiniti, casa dell\'edonismo più sfrenato e quasi sconosciuto ai più.', 'Unico vero locale underground dell\'isola; leggendarie feste di apertura e chiusura; ha lanciato le carriere di alcuni dei migliori DJ di Ibiza; divertimento e giochi folli. Inoltre, si trova alla fine della pista dell\'aeroporto.\r\n\r\n', 'Circoloco, Paradise', 'Circa €10 in taxi da Ibiza città o Playa d\'en Bossa, €30 da San Antonio. L\'autobus per la spiaggia di Salinas si ferma appena oltre il locale. È anche possibile raggiungerla a piedi da San Jordi (circa 15-20 minuti).', 'Per essere famoso è famoso, quanto tutto ciò che c\'è sull\'isola, ma il DC10 ha raggiunto la posizione che ricopre grazie all\'esperienza unica che offre tra le discoteche di Ibiza. Quando fu inaugurato nel 1999, era un luogo di festa senza regole dove ballare fino ad essere sfiniti, casa dell\'edonismo più sfrenato e quasi sconosciuto ai più. A differenza degli altri locali, fa ancora poca o nessuna pubblicità alle sue feste, il che significa che chi ci va, lo fa perché sa cosa può trovare e perchè spinto da un solo vero motivo: la musica. La longevità e il successo del DC10 sono dovuti, in definitiva, al suo impegno nel proporre musica elettronica underground valida e di qualità. Non puoi dire di aver avuto una vera esperienza di Ibiza fino a quando non trascorri un pomeriggio ballando nella famosa terrazza del locale.', 'DC10.jpg'),
(2, 'Amnesia', 'Della vecchia atmosfera hippy che si riunivano nella terrazza a godersi, cantando, dei caldi tramonti... non rimane niente. La terrazza dovettero chiuderla anni fa. Legge sull\'inquinamento acustico, la chiamano. Comunque dentro uno degli impianti musicali piú spettacolari del mondo, insieme a un rinnovato impianto luci. Se uniamo la magia di Ibiza, otteniamo le feste piú incredibili che si possano godere nell\'isola. La Troya, la festa della schiuma o la Matinee riflettono fedelmente la notte piú magica. Uno spettacolo incredibile di cui hanno goduto i piú popolari personaggi della notte d\'Ibiza. Dal Regno Unito si noleggiano aerei con l\'unico obiettivo di scatenarsi nella discoteca Amnesia.', 'Discoteca all\'aperto negli anni \'80; eclettico mix di musica house, disco e pop di DJ Alfredo, negli anni che furono; alba sulla terrazza; le più epiche feste di apertura e chiusura.', 'elrow, Cocoon e Together', ' €10-15 in taxi da San Antonio o Ibiza città, circa €20 euro da Playa d\'en Bossa. I discobus sono in servizio per tutta la notte da Ibiza città, Playa d\'en Bossa e San Antonio e viceversa. Ci sono anche speciali autobus gratuiti per chi possiede i biglietti forniti dalla discoteca o dai promotori che partono da Playa d\'en Bossa, Porto di Ibiza e San Antonio.', 'Senza dubbio la quintessenza delle esperienze clubbing di Ibiza, l\'Amnesia è da molto tempo lo scenario di alcuni dei momenti più indimenticabili dell\'isola. Due enormi sale - la Club Room, un calderone scuro e cavernoso, e la Terrace, un\'ampia arena con tetto di vetro che viene inondata dalla luce naturale al sorgere del sole - sono custodite dai famosi cannoni di ghiaccio dell\'Amnesia che potrebbero colpire in qualsiasi momento. Molto eclettica nella sua programmazione, l\'Amnesia copre il meglio della musica techno, house, trance, dubstep ed organizza persino spettacoli dal vivo. Come tutte le mega-discoteche, le principali piste da ballo possono essere estremamente affollate e sono famose per gli incredibili momenti in cui sulla pista tutti alzano le mani insieme. Il piano superiore dell\'Amnesia è la zona VIP ed offre una vista sorprendente della baldoria che c\'è in pista.', 'Amnesia.jpg'),
(3, 'Space', 'Space Ibiza è la leggendaria discoteca che ha forgiato la cultura del divertimento sull\'isola di Ibiza. Un tempio per gli amanti della musica elettronica. Space Ibiza non è semplicemente una discoteca: è un pezzo di storia della musica elettronica e un simbolo dello stile di vita edonista di Ibiza.', 'Essere uno dei templi della musica elettronica.', 'Armin van Buuren, Afterlife, F*** Me I\'m Famous, Glitterbox e Black Coffee.', '€25 per un taxi da San Antonio, €10 per un taxi da Ibiza Città. I discobus seguono entrambi i percorsi per tutta la notte.', 'Space Ibiza non è semplicemente una discoteca: è un pezzo di storia della musica elettronica e un simbolo dello stile di vita edonista di Ibiza. Nato come un club indipendente alla fine degli anni Ottanta, è diventato in poco tempo un punto di riferimento per i clubbers dell’isola e si è via via espanso fino a diventare un megaspazio votato al divertimento notturno che conta 5 zone tematiche e una terrazza, per una capienza complessiva di 3500 persone. La lista degli artisti internazionali che hanno calcato le scene di Space Ibiza è davvero impressionante e comprende, giusto per fare qualche nome, Fatboy Slim, Chemical Brothers, Underworld, Jeff Mills, Carl Craig, Laurent Garnier, Robert Hood, Francois Kevorkian, Claudio Coccoluto, Richie Hawtin, Dave Clarke, Eric Morillo, Kerri Chandler, Josh Wink, Danny Tenaglia, Frankie Knuckles, Ben Sims. Tra i suoi mitici resident dj il più affezionato è sicuramente l’inglese Carl Cox, una vera leggenda della scena rave dell’isola (tanto da essere soprannominato “il Re di Ibiza”) capace di portare avanti un dj set per 8 ore, infiammando gli animi dei party-animals che continuano a ballare entusiasti. Anche se al momento non sono molte le occasioni per vivere di persone le leggendarie notti dello Space, conoscere l’atmosfera unica di questo locale è imprescindibile per chi vuole vivere in pieno la movida di Ibiza.', 'Space.jpg'),
(4, 'Pacha', 'Pacha ha le proprie discoteche anche a Madrid, Londra, New York, Marrakech e Sharm el-Sheikh. La discoteca Pacha Ibiza può accogliere 3.000 persone e dispone di 5 sale tematiche. La sala principale è quella dove si celebrano le feste più famose e dove Dj internazionali realizzano le loro preformance. Nella parte superiore vi è una terraza con musica più rilassante.', ' Il logo con le ciliegie, i tavoli VIP, il palcoscenico dei ricchi e famosi; inoltre, è la discoteca più antica e famosa di Ibiza.', ' Solomun +1, Flower Power, Music On e Dixon Presents Transmoderna', ' I taxi costano €10 dalla città di Ibiza, €15 da Playa d\'en Bossa, €30 da San Antonio. Puoi raggiungerlo a piedi dalla città di Ibiza in circa 10-15 minuti (ammirando le splendide vedute della Marina lungo la strada); inoltre, i discobus durante l\'alta stagione sono in servizio tutta la notte e si fermano quasi davanti all\'ingresso del Pacha.', 'Costruita ad Ibiza nel 1973, questa antica finca è diventata il brand leader mondiale nel settore del clubbing, con la sede di Ibiza che è ancora oggi il suo fiore all\'occhiello. Con la sua reputazione di club elegante, è impareggiabile quando si tratta di aggiungere un pizzico di verve VIP agli eventi ed è quindi il luogo ideale per indossare i tuoi abiti migliori prima di ballare con alcuni dei più grandi DJ del mondo. Il Pacha è indissolubilmente legato ad Ibiza – è un club la cui reputazione nel corso degli anni è cresciuta di pari passo con la reputazione dell\'isola stessa. Di conseguenza, è un locale molto caro alla gente del posto ed è considerato una \"visita obbligata\" per i turisti che vengono per la prima volta nell\'Isola. Nonostante gli oltre quarant\'anni di feste al suo attivo, può ancora vantare una delle scuderie musicali con più talenti dell\'intera Isla Blanca, nonchè uno degli interni più moderni in quanto interamente ristrutturato nel 2018. Scegli tra la crème della musica elettronica, dall\'underground alla musica di tendenza e ai classici pop del passato.', 'Pacha.jpg'),
(7, 'BoatParty', 'Partecipa alla festa in barca più divertente di Ibiza con DJ, fotografo professionale, attività acquatiche, open bar... Tutti gli ingredienti per vivere una giornata indimenticabile, persino uno schiuma party!', 'Goditi un open bar comprensivo di bibite analcoliche, birre e cocktail \"sex on the boat\" illimitati\r\nUna volta a riva continua la festa con l\'ingresso a club selezionati\r\nVantati di un\'esperienza VIP a Ibiza con una festa al tramonto in barca\r\nIngresso per 5 eventi in 1 biglietto: una festa in spiaggia, una festa in barca e almeno 3 ingressi per Club', 'A Sant Antoni, Sa Caleta, Cala Compte.', 'A nuoto.', 'Lo scopo è semplice: partecipa alla festa in barca più pazza e all-inclusive. Non perderti le docce di champagne più grandi di Ibiza, 4 ore di divertimento senza limiti, avventure indimenticabili e momenti magici. Prendi subito i biglietti.', 'BoatParty.jpg'),
(8, 'FesteInVilla', 'Prenota un villa per la tua festa personale, con possibilità di avere il dj personale e uno staff completo.', ' Per le location indimenticabili.', ' I migliori after party d\'Europa.', ' Taxi privati a 30€.', ' Stai organizzando un evento importante? Oppure si avvicina una ricorrenza che vuoi festeggiare alla grande? Scegli una delle ville per feste disponibili su ibizaByNight.it: residenze di lusso e spaziose, perfette per ospitare qualsiasi festa. Se abiti in una grande città e vuoi celebrare un anniversario di matrimonio, le ville per feste in provincia sono la location più adatta. A Sant\'Antoni puoi trovare molti casali ristrutturati e ville d’epoca che presentano tutte le caratteristiche di cui hai bisogno: sale raffinate da allestire con tavoli e buffet, per un pranzo o una cena a cinque stelle. Puoi anche optare di festeggiare al caldo, scegliendo una delle ville per feste in spiaggia: avrai così la possibilità di intrattenere parenti e amici in una villa in riva al mare oppure in una delle tradizionali finche Ibizenche. Davanti a un paesaggio suggestivo come quello di Ibiza, qualsiasi celebrazione assume un tono ancora più importante e poetico, specialmente con un brindisi vista mare al tramonto.', 'FesteInVilla.jpg');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`PK_club`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `club`
--
ALTER TABLE `club`
  MODIFY `PK_club` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 16, 2020 alle 15:59
-- Versione del server: 10.4.8-MariaDB
-- Versione PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibizabynight`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `organizzatore`
--

CREATE TABLE `organizzatore` (
  `PK_organizzatore` int(11) NOT NULL,
  `Nome` varchar(20) NOT NULL,
  `Cognome` varchar(20) NOT NULL,
  `Username` varchar(12) NOT NULL,
  `Password` varchar(128) NOT NULL,
  `Email` varchar(50) NOT NULL DEFAULT 'latuamail@mail.com',
  `Salt` varchar(128) NOT NULL,
  `Immagine` varchar(100) NOT NULL,
  `CodiceSicurezza` varchar(10) NOT NULL,
  `list_comment` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `organizzatore`
--

INSERT INTO `organizzatore` (`PK_organizzatore`, `Nome`, `Cognome`, `Username`, `Password`, `Email`, `Salt`, `Immagine`, `CodiceSicurezza`, `list_comment`) VALUES
(15, 'Fabrizio ', 'De meis', 'fabric', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'prova@gmail.com', 'b3de98a4b5609e8c1dc45d21e874446dcbdc93a751b475d60ee55e95a9b1ee0057a282ce8fee7e2e2a59da9a6119cbb85b54c695f86e4abcb64058acb1c729f1', 'DefaultProfilo.jpg', '5mjR7bDH0c', '164/163/162/161/160/159/'),
(16, 'Enrico', 'Roncuzzi', 'Admin', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'prova@gmail.com', '7a72402987f6f41581c8b930a841afc4aa991eae647e0af45069891d9b0ed8eaa4b07ebbeec318691a7ace3c10be2c25bfd6832f4679399d93981e29c09c51db', 'gabibbo.jpg', 'DkkP1XcQjA', ''),
(17, 'Enrico', 'Roncuzzi', 'cappo', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'prova@gmail.com', '1cdd9bb510241140f4f17954603ad29f5720b35cf9437507869f3e4355ecb511ae0be3775d525c4813ea09be8793f85ff085e63f1e075cb59dce8b7a09d9a0e7', 'DefaultProfilo.jpg', 'is1nU40nVv', ''),
(18, 'Enrico', 'Roncuzzi', 'wow', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'prova@gmail.com', '7ca5dd4b1e98bdcff8bf73ed03bd6797e3f5a774fb29e91355a7beaa03af82fb4257d19fa446662a92a90b114eb3d710d578c53c061e81d0e41f163ed5129e58', 'DefaultProfilo.jpg', 'W4MdHNIY0w', '');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `organizzatore`
--
ALTER TABLE `organizzatore`
  ADD PRIMARY KEY (`PK_organizzatore`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `organizzatore`
--
ALTER TABLE `organizzatore`
  MODIFY `PK_organizzatore` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

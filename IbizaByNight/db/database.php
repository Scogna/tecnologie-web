<?php
class DatabaseHelper
{
    private $db;

    public function __construct()
    {

        //eKcGZr59zAa2BEWU
        define("HOST", "localhost");
        define("USER", "root");
        define("PASSWORD", "");
        define("DATABASE", "ibizabynight");
        $this->db = new mysqli(HOST, USER, PASSWORD, DATABASE);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }
    }

    public function checkLogin($username, $password)
    {
        $query = "SELECT PK_utente, Username, Nome FROM utente WHERE  Username = ? AND Password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function insertUtent($nome, $cognome, $email, $password, $username, $salt, $img, $codsic)
    {
        $query = "INSERT INTO utente (nome, cognome, email, password, username, salt, Immagine, CodiceSicurezza) VALUES (?, ?, ?, ?, ?, ?, ?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssssss', $nome, $cognome, $email, $password, $username, $salt, $img, $codsic);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertOrg($nome, $cognome, $email, $password, $username, $salt, $img, $codsic)
    {
        $query = "INSERT INTO organizzatore (nome, cognome, email, password, username, salt, Immagine,CodiceSicurezza) VALUES (?, ?, ?, ?, ?, ?, ?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssssss', $nome, $cognome, $email, $password, $username, $salt, $img, $codsic);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertEvent($nome, $descrizione, $prezzo, $partecipanti, $ospite, $data, $club, $img, $owner)
    {
        $query = "INSERT INTO eventi (nome, descrizione, prezzo, partecipanti, ospite, data, club , Immagine, InseritoDa) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssiisssss', $nome, $descrizione, $prezzo, $partecipanti, $ospite, $data, $club, $img, $owner);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertEventCart($nome, $descrizione, $prezzo, $partecipanti, $ospite, $data, $club, $img, $username)
    {
        $query = "INSERT INTO carrello (nome, descrizione, prezzo, partecipanti, ospite, data, club , Immagine, Username) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssiisssss', $nome, $descrizione, $prezzo, $partecipanti, $ospite, $data, $club, $img, $username);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function insertInCart($username, $evento)
    {
        $query = "INSERT INTO acquisto (Evento, Acquirente ) VALUES (?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('is', $evento, $username);
        $stmt->execute();

        return $stmt->insert_id;
    }



    public function getmyDB()
    {
        if ($this->db != null) {
            return $this->db;
        }
    }

    public function getClub()
    {
        $stmt = $this->db->prepare("SELECT * FROM club");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEvent()
    {
        $stmt = $this->db->prepare("SELECT * FROM eventi");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getIdItemCartByUser($username)
    {
        $stmt = $this->db->prepare("SELECT Evento FROM acquisto WHERE Acquirente=?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRandomClub($n)
    {
        $stmt = $this->db->prepare("SELECT PK_club, Nome, Descrizione FROM club ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i', $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventsbyClub($club)
    {
        $stmt = $this->db->prepare("SELECT * FROM eventi WHERE club=?");
        $stmt->bind_param("s", $club);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByPk($pk)
    {
        $stmt = $this->db->prepare("SELECT PK_evento, Nome, Descrizione, Prezzo, Partecipanti, Ospite, Club, Data, Immagine, InseritoDa, Approvato FROM eventi WHERE Pk_evento=? ");
        $stmt->bind_param("i", $pk);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_assoc();
    }



    public function getRandomEvent($n)
    {
        $stmt = $this->db->prepare("SELECT PK_evento, Nome, Descrizione, Prezzo, Partecipanti, Ospite, Club, Data, Immagine, InseritoDa, Approvato FROM eventi ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i', $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkUser($username, $password)
    {
        $query = "SELECT  nome, PK_utente, username, Email, Cognome password FROM utente WHERE username=? AND password=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkOrg($username, $password)
    {
        $query = "SELECT nome, PK_organizzatore, username, Email, Cognome password FROM organizzatore WHERE username=? AND password=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function updateQnt($qnt, $pk)
    {
        $query = "UPDATE acquisto SET Quantità = ? WHERE evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $qnt, $pk);

        return $stmt->execute();
    }

    public function deleteItemFromCart($pk)
    {
        $query = "DELETE FROM acquisto WHERE Evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $pk);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function BuyTicket($PKevento, $username, $data)
    {
        $query = "INSERT INTO eventiacquistati (PK_evento, Username, Data)  VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("iss", $PKevento, $username, $data);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function UpDatePlace($place, $PKevento)
    {
        $query = "UPDATE eventi SET Partecipanti = ? WHERE PK_evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $place, $PKevento);
        return $stmt->execute();
    }

    public function getUserByPk($pk)
    {
        $stmt = $this->db->prepare("SELECT Nome, Cognome, Email, Username, Immagine FROM utente WHERE PK_utente=? ");
        $stmt->bind_param("i", $pk);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOrgByPk($pk)
    {
        $stmt = $this->db->prepare("SELECT Nome, Cognome, Email, Username, Immagine FROM organizzatore WHERE PK_organizzatore=? ");
        $stmt->bind_param("i", $pk);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByUser($username)
    {
        $stmt = $this->db->prepare("SELECT PK_evento FROM eventiacquistati WHERE username=? ");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventByOrg($username)
    {
        $stmt = $this->db->prepare("SELECT PK_evento FROM eventi WHERE InseritoDa=? ");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventQtnByUser($username, $PKevento)
    {
        $stmt = $this->db->prepare("SELECT Quantita FROM eventiacquistati WHERE username=? AND PK_evento=?");
        $stmt->bind_param("si", $username, $PKevento);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_assoc();
    }
    public function UpDateQtnByUs($username, $quantita, $PKevento)
    {
        $query = "UPDATE eventiacquistati SET Quantita = ? WHERE username = ? AND PK_evento=? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isi', $quantita, $username, $PKevento);
        return $stmt->execute();
    }


    public function UpDatePasswordUtent($username, $password, $codsic)
    {
        $query = "UPDATE utente SET Password = ? WHERE CodiceSicurezza=? AND Username =? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sss', $password, $codsic, $username);
        return $stmt->execute();
    }

    public function UpDatePasswordOrg($username, $password, $codsic)
    {
        $query = "UPDATE organizzatore SET Password = ? WHERE CodiceSicurezza=? AND Username =? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sss', $password, $codsic, $username);
        return $stmt->execute();
    }

    public function CheckUsernameUtent($username)
    {
        $query = "SELECT username FROM utente WHERE username=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function CheckUsernameOrg($username)
    {
        $query = "SELECT username FROM organizzatore WHERE username=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function UpDateEvent($Pk_evento, $nome, $data, $descrizione, $prezzo, $ospite, $club)
    {
        $query = "UPDATE eventi SET Nome = ?, Descrizione = ?, Prezzo = ?,  Ospite = ?, Data =?, Club=? WHERE PK_evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssisssi', $nome, $descrizione, $prezzo, $ospite, $data, $club, $Pk_evento);
        return $stmt->execute();
    }


    public function CheckCodiceSicurezzaUtent($codsic)
    {
        $query = "SELECT CodiceSicurezza FROM utente WHERE CodiceSicurezza=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $codsic);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function CheckCodiceSicurezzaOrg($codsic)
    {
        $query = "SELECT CodiceSicurezza FROM organizzatore WHERE CodiceSicurezza=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $codsic);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    public function insertMessage($sub, $comm, $sender, $obj, $data)
    {
        $query = "INSERT INTO comments (comment_subject	, comment_text, PK_sender, comment_object,comment_data) VALUES (?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssiss', $sub, $comm, $sender, $obj, $data);
        $stmt->execute();

        return $stmt->insert_id;
    }
    public function getMessage()
    {
        $stmt = $this->db->prepare("SELECT comment_id,comment_subject	, comment_text, PK_sender, comment_object,comment_data FROM comments");
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCommStatus($com, $nomEv)
    {
        $stmt = $this->db->prepare("SELECT comment_status FROM comments WHERE comment_status=? AND comment_subject=?");
        $stmt->bind_param("is", $com, $nomEv);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function setCommStatus($s, $id)
    {
        $query = "UPDATE comments SET comment_status=? WHERE comment_id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $s, $id);
        return $stmt->execute();
    }

    public function insertClub($nome, $descrizione, $descrizionDett, $famosaPer, $festeFamose, $comeArrivare, $img)
    {
        $query = "INSERT INTO club (nome, descrizione, DescrDett, Famosaper, FesteFamose, ComeArrivare, immagine) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssss', $nome, $descrizione, $descrizionDett, $famosaPer, $festeFamose, $comeArrivare, $img);
        $stmt->execute();

        return $stmt->insert_id;
    }
    //inizio func commenti letti

    public function getCommReadUt($pk)
    {
        $query = "SELECT list_comment FROM utente WHERE pk_utente=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $pk);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function getCommReadOr($pk)
    {
        $query = "SELECT list_comment FROM organizzatore WHERE pk_organizzatore=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $pk);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }
    public function insertCommReadUt($id, $pk)
    {
        $query = "UPDATE utente SET list_comment=? WHERE pk_utente=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $id, $pk);
        $stmt->execute();

        return $stmt->insert_id;
    }
    public function insertCommReadOr($id, $pk)
    {
        $query = "UPDATE organizzatore SET list_comment=? WHERE pk_organizzatore=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $id, $pk);
        $stmt->execute();

        return $stmt->insert_id;
    }


    //fine func commenti letti 

    public function UpDateClub($nome, $descrizionDett, $famosaPer, $festeFamose, $comeArrivare, $pk_club)
    {
        $query = "UPDATE club SET Nome = ?, DescrDett = ?, famosaPer = ?,  festeFamose = ?, comeArrivare =? WHERE PK_club = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sssssi', $nome, $descrizionDett, $famosaPer, $festeFamose, $comeArrivare, $pk_club);
        return $stmt->execute();
    }

    public function deleteClub($pk)
    {
        $query = "DELETE FROM club WHERE PK_club = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $pk);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function approva($Pk_evento)
    {
        $appr = true;
        $query = "UPDATE eventi SET Approvato = ? WHERE PK_evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $appr, $Pk_evento);
        return $stmt->execute();
    }

    public function getEventApprov()
    {
        $appr = true;
        $query = "SELECT * FROM eventi WHERE Approvato=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $appr);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEventNotApprov()
    {
        $appr = false;
        $query = "SELECT * FROM eventi WHERE Approvato=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $appr);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteEvent($pk)
    {
        $query = "DELETE FROM eventi WHERE PK_evento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $pk);
        $stmt->execute();
        var_dump($stmt->error);
        return true;
    }

    public function getQuant($pk, $username){
        $query = "SELECT Quantita FROM eventiacquistati WHERE PK_evento=? AND Username=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('is', $pk,$username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }
}

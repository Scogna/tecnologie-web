<?php
require_once 'connection.php';

$templateParams["nav"] = "nav.php";
$templateParams["clubs"] = $dbh->getClub();
$templateParams["EventiApprovati"] = $dbh->getEventApprov();



if (isset($_SESSION["PK_utente"])) {
    $templateParams["dati_utente"] = $dbh->getUserByPk($_SESSION["PK_utente"]);
    $templateParams["feste_attive"] = $dbh->getEventByUser($_SESSION["username"]);
} else if (isset($_SESSION["PK_organizzatore"])) {
    $templateParams["dati_utente"] = $dbh->getOrgByPk($_SESSION["PK_organizzatore"]);
    $templateParams["feste_attive"] = $dbh->getEventByOrg($_SESSION["username"]);

}
$numMessage=0;
$items= $templateParams["feste_attive"];
//$_SESSION["test"] = $templateParams["feste_attive"];
$party = array();
if(isset($items)){
foreach($items as $item){
    foreach($item as $it){
        $festa=$dbh->getEventByPk($it);
        $v=$dbh->getCommStatus(0,$festa["Nome"]);
        array_push($party,$dbh->getEventByPk($it));
    }
    $numMessage=$numMessage+count($v);
        
};
}
$num_messaggi_totali=$numMessage+1;
if (isset($_SESSION["PK_utente"])) {
    $messaggi_letti=$dbh->getCommReadUt($_SESSION["PK_utente"]);
    $arr_messaggi_visti = explode('/',$messaggi_letti["list_comment"]);
    $num_messaggi_letti=count($arr_messaggi_visti);
    $num_mex_davisual=$num_messaggi_totali-$num_messaggi_letti;
}else{
    $messaggi_letti=$dbh->getCommReadOr($_SESSION["PK_organizzatore"]);
    $arr_messaggi_visti = explode('/',$messaggi_letti["list_comment"]);
    $num_messaggi_letti=count($arr_messaggi_visti);
    $num_mex_davisual=$num_messaggi_totali-$num_messaggi_letti;
}
//fine conteggio messaggi
$templateParams["EventiAdminApprovati"] = "Template/EventiApprovati.php";

$templateParams["array"] = $party;
$templateParams["events_active"] = "Template/EventiAttivi.php";
$templateParams["events_past"] = "Template/EventiPassati.php";
$templateParams["Numero_messaggi"] =  $num_mex_davisual;

require 'Template/AreaPrivataClient.php';
?>
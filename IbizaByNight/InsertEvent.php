<?php
require_once 'connection.php';

$templateParams["nav"] = "nav.php";
$templateParams["clubs"] = $dbh->getClub();



   

if(isset($_POST["NomeEvento"]) && isset($_POST["Descrizione"]) && 
    isset($_POST["Prezzo"]) && isset($_POST["Partecipanti"]) && isset($_POST["Ospite"]) && 
    isset($_POST["data"]) && isset($_POST["club"]) ){
    $nome = $_POST["NomeEvento"];
    $descrizione = $_POST["Descrizione"];
    $prezzo = $_POST["Prezzo"];
    $partecipanti = $_POST["Partecipanti"];
    $ospite = $_POST["Ospite"];
    $data = $_POST["data"];
    $club= $_POST["club"];
    if(isset($_FILES["imgEvento"])){
        list($result, $msg) =  uploadImage(UPLOAD_DIR, $_FILES["imgEvento"]);
        if($result != 0){
            $img = $msg;
        }else{
            $img= "defaultEvent.jpg";
       }
    }
   
    $dbh->insertEvent($nome,$descrizione,$prezzo,$partecipanti,$ospite,$data,$club, $img, $_SESSION["username"]);
    header("location: AreaPrivataClient.php");

}
    
require 'Template/InsertEvent.php';
?>
<?php
require_once 'connection.php';
$templateParams["array_comm_visti"]=0;
$templateParams["nav"] = "Template/nav.php";
if (isset($_SESSION["PK_utente"])) {
    $templateParams["dati_utente"] = $dbh->getUserByPk($_SESSION["PK_utente"]);
    $templateParams["feste_attive"] = $dbh->getEventByUser($_SESSION["username"]);
} else if (isset($_SESSION["PK_organizzatore"])) {
    $templateParams["dati_utente"] = $dbh->getOrgByPk($_SESSION["PK_organizzatore"]);
    $templateParams["feste_attive"] = $dbh->getEventByOrg($_SESSION["username"]);
}
//array con le feste attive (e tutti i rel. parametri)
$items = $templateParams["feste_attive"];
$party = array();
if (isset($items)) {
    foreach ($items as $item) {
        foreach ($item as $it) {
            $nomi_feste = $dbh->getEventByPk($it);
            array_push($party, $nomi_feste["Nome"]);
        }
    };
}
//var_dump($party);

//--
//array con tutti i commenti che mi servono (ovvero delle feste attive)
$templateParams["list_message"] = $dbh->getMessage();
$items = $templateParams["list_message"];
$com = array();
if (isset($items)) {
    foreach ($items as $item) {
        if (in_array($item["comment_subject"], $party)) array_push($com, $item);
    }
}
rsort($com);
//--



//inserisco commento già letto

if (isset($_GET["Id"])) {
    $bool = 0;
    if (isset($_SESSION["PK_utente"])) {
        $letti = $dbh->getCommReadUt($_SESSION["PK_utente"]);
        //controllo degli elementi
        $arr_commenti_visti = explode('/', $letti["list_comment"]);
        foreach ($arr_commenti_visti as $cv) {
            if ($cv == $_GET["Id"]) $bool = 1;
        };
        //--
        if ($bool == 0) {
            $letti["list_comment"] = $letti["list_comment"] . $_GET["Id"] . "/";
            $dbh->insertCommReadUt($letti["list_comment"], $_SESSION["PK_utente"]);
        }
    } else {
        $letti = $dbh->getCommReadOr($_SESSION["PK_organizzatore"]);
        //controllo degli elementi
        $arr_commenti_visti = explode('/', $letti["list_comment"]);
        foreach ($arr_commenti_visti as $cv) {
            if ($cv == $_GET["Id"]) $bool = 1;
        };
        //--
        if ($bool == 0) {
            $letti["list_comment"] = $letti["list_comment"] . $_GET["Id"] . "/";
            $dbh->insertCommReadOr($letti["list_comment"], $_SESSION["PK_organizzatore"]);
        }
    }
}
//evidenziato o non
if (isset($_SESSION["PK_utente"])) {
    $letti = $dbh->getCommReadUt($_SESSION["PK_utente"]);
    if( $letti["list_comment"]==0)$templateParams["array_comm_visti"]=0;
    else $templateParams["array_comm_visti"]=explode('/', $letti["list_comment"]);
} else {
    $letti = $dbh->getCommReadOr($_SESSION["PK_organizzatore"]);
    if( $letti["list_comment"]==0)$templateParams["array_comm_visti"]=0;
    else $templateParams["array_comm_visti"]=explode('/', $letti["list_comment"]);
}


$templateParams["array1"] = $com;
$templateParams["messaggi"] = "Template/Mail.php";
require 'Template/BoxMail.php';

<!DOCTYPE html>
<html lang="en" >

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Amnesia</title>

  <!-- Bootstrap core CSS -->
  <link href="js/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="js/modern-business.css" rel="stylesheet">

</head>

<body style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" background="upload/Download.jpg">

  <!-- Navigation -->
  <?php
    if(isset($templateParams["nav"])){
        require($templateParams["nav"]);
    }
    ?>

  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3" style="background-color: white; width:15%">Amnesia</h1>
      <hr>
      <!-- Section:botton bar -->
      <div class="row">
        <div class="col-md-12">
          <div class="btn-group" role="group" aria-label="Basic example">
            
            <?php if (isset($_SESSION["PK_organizzatore"])) : ?>
              <a type="button" <?php isActive("InsertEvent.php"); ?> href="InsertEvent.php" class="btn btn-secondary">Registra il tuo evento</a>
        <?php endif; ?>
        </div>
      </div>
    </div>
<hr>
      <h1><small>Tutti gli eventi</small></h1>
    

    <div class="row">
      <?php
    if(isset($templateParams["Evento"])){
        require($templateParams["Evento"]);
    }
    ?>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
  <hr>
  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>

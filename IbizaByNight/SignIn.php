<?php
require_once 'connection.php';


if (isset($_POST["Nome"]) && isset($_POST["Cognome"]) && isset($_POST["Username"]) && isset($_POST["Email"]) && isset($_POST["Password"]) && isset($_POST["ConfermaPassword"])) {
    if ($_POST["Password"] != $_POST["ConfermaPassword"]) {
        $templateParams["msg"] = "Le password non corrispondono!";
    } else if (count($dbh->CheckUsernameUtent($_POST["Username"])) || count($dbh->CheckUsernameOrg($_POST["Username"]))) {

        $templateParams["msg"] = "Username Gia esistente";
    } else {

        $check = isset($_POST['check']) ? $_POST['check'] : 'no';
        $nome = $_POST["Nome"];
        $cognome = $_POST["Cognome"];
        $username = $_POST["Username"];
        $email = $_POST["Email"];
        $password = $_POST["Password"];
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
        $password = hash('sha512', $password);
        $codiceSicurezza = generaStringaRandom(10);
        if(isset($_FILES["ImgProfilo"])){
            list($result, $msg) =  uploadImage(UPLOAD_DIR, $_FILES["ImgProfilo"]);
            if($result != 0){
                $img = $msg;
            }else{
                $img= "DefaultProfilo.jpg";
           }
        }
       


        if ($check == "no") {

            $dbh->insertUtent($nome, $cognome, $email, $password, $username, $random_salt, $img, $codiceSicurezza);
            header("location: Login.php");
        } else {
            $dbh->insertOrg($nome, $cognome, $email, $password, $username, $random_salt, $img, $codiceSicurezza);
            header("location: Login.php");
        }
    }
}



$templateParams["nav"] = "nav.php";

require 'Template/SignIn.php';

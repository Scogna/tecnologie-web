<?php
require_once 'connection.php';

if(!isset($_SESSION["username"])){
    header("location: Login.php");
}
if(isset($_GET["Evento"])){
   $dbh->insertInCart($_SESSION["username"],$_GET["Evento"]);
}

$templateParams["nav"] = "nav.php";

$items= $dbh->getIdItemCartByUser($_SESSION["username"]);
$itemCart = array();
if(isset($items)){
    foreach($items as $item){
        foreach($item as $it){
        array_push($itemCart,$dbh->getEventByPk($it));
    }
    };
}




$templateParams["EventiNelCarrello"] = "Template\ItemCart.php";
$templateParams["EventiCarrello"] = $itemCart;

require 'Template/Cart.php';
?>
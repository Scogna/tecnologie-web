<?php $infomodal = 0;
$inApprov = "a"; ?>

<?php foreach ($templateParams["array"] as $Festa) : ?>
  <?php if ((GetDay($Festa["Data"]) == true)) : ?>
    <?php $infomodal++;
    $inApprov++ ?>
    <div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" id="cartaEventi" class="col-lg-3 mb-4">
      <div class="card h-100">
        <a><img class="card-img-top" <?php if (isset($Festa["Immagine"])) : ?> src="<?php echo UPLOAD_DIR . $Festa["Immagine"]; ?>" <?php endif; ?> style=" width: 100%; height: 150px" alt="Immagine evento"></a>
        <h4 class="card-header"><strong><?php echo $Festa["Nome"]; ?></strong> </h4>
          <?php  $quantita = $dbh->getQuant($Festa["PK_evento"],$_SESSION["username"]); ?>
        <div class="card-body">
          <p class="card-text"><strong>Descrizione:</strong> <?php echo $Festa["Descrizione"]; ?></p>
          <p class="card-text"><strong>Prezzo:</strong> <?php echo $Festa["Prezzo"] . "€"; ?></p>
          <p class="card-text"><strong>Ospite:</strong> <?php echo $Festa["Ospite"]; ?></p>
          <p class="card-text"><strong>Data:</strong> <?php echo $Festa["Data"]; ?></p>
          <p class="card-text"><strong>Dove:</strong> <?php echo $Festa["Club"]; ?></p>
          <p class="card-text"><strong>Oraganizzato da:</strong> <?php echo $Festa["InseritoDa"]; ?></p>
          <?php if (isset($_SESSION["PK_utente"])) : ?>
          <p class="card-text"><strong>Numero biglietti acquistati: </strong><?php echo $quantita["Quantita"]; ?></p>
          <?php endif; ?> 
          <?php if (isset($_SESSION["PK_organizzatore"])) : ?>
          <p class="card-text"><strong>Biglietti Residui: </strong><?php echo $Festa["Partecipanti"]  ?></p>
          <?php endif; ?>

        </div>


        <?php if (isset($_SESSION["PK_organizzatore"]) && ($Festa["Approvato"] == true) && (!isset($_SESSION["Admin"]))) : ?>
          <div class="card-footer text-center">
            <a class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-toggle="modal" style="border-color: white; color: white;" data-target="#_<?php echo $infomodal; ?>">Modifica</a></div>
        <?php endif; ?>
        <?php if (isset($_SESSION["PK_organizzatore"]) && ($Festa["Approvato"] == false) && (!isset($_SESSION["Admin"]))) : ?>
          <div class="card-footer text-center">
            <a class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-toggle="modal" style="border-color: white; color: white;" data-target="#_<?php echo $inApprov; ?>">In Approv.</a></div>
        <?php endif; ?>

        <?php if (isset($_SESSION["Admin"]) && ($Festa["Approvato"] == false)) : ?>
          <div class="card-footer text-center">
            <a class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-toggle="modal" style="border-color: white; color: white;" data-target="#_<?php echo $inApprov; ?>">Approva</a></div>
        <?php endif; ?>


        <div class="modal fade" id="_<?php echo $infomodal ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modifica le informazioni di <?php echo $Festa["Nome"] ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form method="POST" action="<?php echo 'Modify.php?Evento=' . $Festa["PK_evento"] ?>">
                  <div class="form-group">
                    <label for="message-text" class="col-form-label"> <strong> Nome Evento: </strong> </label>
                    <input type="text" id="NomeEvento" name="Nome" class="form-control" value="<?php echo $Festa["Nome"] ?>">
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="col-form-label"> <strong> Descrizione: </strong> </label>
                    <textarea class="form-control" rows="4" name="Descrizione" id="message-text"> <?php echo $Festa["Descrizione"]; ?></textarea>
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="col-form-label"> <strong> Prezzo: </strong> </label>
                    <input type="number" id="Prezzo" name="Prezzo" class="form-control" value="<?php echo $Festa["Prezzo"]; ?>">
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="col-form-label"> <strong> Ospite: </strong> </label>
                    <input type="textarea" id="Ospite" name="Ospite" class="form-control" value="<?php echo $Festa["Ospite"]; ?>">
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="col-form-label"> <strong> Data: </strong> </label>
                    <input type="date" id="Data" name="Data" class="form-control" value="<?php echo $Festa["Data"]; ?>">
                  </div>
                  <div class="form-group">
                    <label for="sel1"><strong>Dove:</strong> </label>
                    <select name="Club" class="form-control" id="sel1">
                      <?php foreach ($templateParams["clubs"] as $club) : ?>
                        <option><?php echo $club["Nome"]; ?></option>
                      <?php endforeach; ?>
                    </select>

                  </div>

              </div>
              <div class="modal-footer text-center">
                <input value="Chiudi" type="button" class="btn btn-lg btn-primary text-uppercase bg-dark" style="border-color: white;" data-dismiss="modal"></input>
                <input value="Modifica" class="btn btn-lg btn-primary text-uppercase bg-dark" style="border-color: white;" type="submit"></input>
              </div>
            </div>
          </div>
          </form>
        </div>


        <!-- Modale aprrov-->
        <div id="_<?php echo $inApprov ?>" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">In Approvazione</h4>
              </div>
              <div class="modal-body">
                <p><?php echo $_SESSION["username"]; ?> L'evento è in attesa di essere approvato</p>
              </div>
              <div class="modal-footer">
                <button type="button" style="border-color: white;" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-dismiss="modal">ok</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Fine Modale -->

      </div>
    </div>
  <?php endif; ?>
<?php endforeach; ?>
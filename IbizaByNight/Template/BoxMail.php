<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Messaggi</title>

  <!-- Bootstrap core CSS -->
  <link href="js/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/bootstrap/css/MyCustom.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="js/modern-business.css" rel="stylesheet">

</head>

<!-- Modale -->
<?php
if (isset($templateParams["modale"])) {
  require($templateParams["modale"]);
}
?>
<!-- Fine Modale -->

<body>

  <!-- Navigation -->
  <?php
  if (isset($templateParams["nav"])) {
    require($templateParams["nav"]);
  }
  ?>

  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <?php if (isset($_SESSION["PK_utente"])) : ?>
      <h1 class="mt-4 mb-3">Messaggi Ricevuti</h1>
    <?php endif; ?>
    <?php if (isset($_SESSION["PK_organizzatore"])) : ?>
      <h1 class="mt-4 mb-3">Messaggi Inviati</h1>
    <?php endif; ?>
    <!-- Inserisco le info -->
    <div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" class="mail-box">
      <aside class="lg-side">
        <div class="inbox-body">
          <table class="table table-inbox table-hover">
            <tbody>
              <tr class="unread">

                <td class="table td ">Data: </td>
                <td class="table-object">Oggetto:</td>
                <td class="table td ">Proveniente da:</td>
                 <!-- <td class="view-message  inbox-small-cells"><i class="fa fa-paperclip"></i></td>-->
                <td class="table td"></td>

              </tr>
            </tbody>
          </table>
        </div>
      </aside>
    </div>
    <!-- Fine inserimento info -->


    <!-- Inserisco tutti i messaggi -->
    <div>
      <?php
      if (isset($templateParams["messaggi"])) {
        require($templateParams["messaggi"]);
      }
      ?>
    </div>

    <!-- Fine inserimento messaggi -->
  </div>




  </div>
  <!-- /.row -->

  </div>
  <!-- /.container -->




  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; IbizaByNight</p>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>
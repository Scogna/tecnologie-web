<!DOCTYPE html>
<html lang="it">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login</title>

  <!-- Bootstrap core CSS -->
  <link href="js/bootstrap.min.css" rel="stylesheet">



  <!-- Custom styles for this template -->
  <link href="js/modern-business.css" rel="stylesheet">

</head>

<body style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;">

  <!-- Navigation -->
  <?php
    if(isset($templateParams["nav"])){
        require($templateParams["nav"]);
    }
    ?>

  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Accedi
      <?php if(isset($templateParams["errorelogin"])): ?>
      <p><?php echo $templateParams["errorelogin"]; ?></p>
      <?php endif; ?>
    </h1>





    <!-- Marketing Icons Section -->
    <div class="container-fluid">
      <div class="row no-gutter">
        <div class="d-none d-md-flex col-md-4 col-lg-6 ">
        </div>
        <div class="col-md-8 col-lg-6">
          <div class="login d-flex align-items-center py-5">
            <div class="container">
              <div class="row">
                <div class="col-md-9 col-lg-8 mx-auto">
                  <h3 class="login-heading mb-4">Ben Tornato!</h3>
                  <form action="Login.php" method="POST" name="Login">

                    <div class="form-label-group">
                      <input type="text" id="Username" name="Username" class="form-control" placeholder="Username" required autofocus>
                      <label for="Username">Username</label>
                    </div>

                    <div class="form-label-group">
                      <input type="password" id="Password" name="Password" class="form-control" placeholder="Password" required>
                      <label for="Password">Password</label>
                    </div>
                    <input class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" style="border-color: white;"value="Accedi" onclick="formhash(this.form, this.form.Password);" type="button"></input>
                    <div class="text-center">
                      <p><a class="small" <?php isActive("SignIn.php");?> href="SignIn.php">Registrati</a></p>
                      </form>
                    </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->
  <!-- Footer -->
  <footer class="py-5 bg-dark">
      <p class="m-0 text-center text-white">Copyright &copy; IbizaByNight</p>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script type="text/javascript" src="vendor/bootstrap/js/sha512.js"></script>
  <script type="text/javascript" src="vendor/bootstrap/js/form.js"></script>

</body>

</html>

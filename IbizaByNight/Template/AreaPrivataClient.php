<!------ Include the above in your HEAD tag ---------->
<?php foreach ($templateParams["dati_utente"] as $Dati) : ?>
  <!DOCTYPE html>
  <html  style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Area Privata</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">

  </head>

  <!-- Navigation -->

  <div>
    <?php
    if (isset($templateParams["nav"])) {
      require($templateParams["nav"]);
    }
    ?>
  </div>
  <!-- Navigation -->

  <body>
  <div class="container">
    <!-- ******HEADER****** -->
    <header class="header">
     
        <div class="teacher-name" style="padding-top:20px;">

          <div class="row" style="margin-top:0px;">
            <div class="col-md-9">
              <h2 style="font-size:38px"><strong>Area privata</strong></h2>
            </div>
            <div class="col-md-3">
            </div>
          </div>
        </div>

        <div class="row" style="margin-top:20px;">
          <div class="col-md-3">
            <!-- Image -->
            <a href="#"> <img class="img-fluid rounded mb-3 mb-md-0" <?php if (isset($Dati["Immagine"])) : ?> src="<?php echo UPLOAD_DIR . $Dati["Immagine"]; ?>" <?php endif; ?> alt="Immagine Profilo"></img>
            </a>
          </div>

          <div class="col-md-6">
            <!-- Rank & Qualifications -->
            <h5>Nome: <small><?php echo $Dati["Nome"] ?></small></h5>
            <h5>Cognome: <small><?php echo $Dati["Cognome"] ?></small></h5>
            <h5>E-mail: <small><?php echo $Dati["Email"] ?></small></h5>
            <h5>Username: <small><?php echo $Dati["Username"] ?></small></h5>




          </div>

        </div>
        <hr>
      </div>
    </header>
    <!--End of Header-->

    <!-- Main container -->
    <div class="container">

      <!-- Section:botton bar -->
      <div class="row">
        <div class="col-md-12">
          <div class="btn-group" role="group" aria-label="Basic example">
            <?php if (isset($_SESSION["PK_organizzatore"])) : ?>
              <a type="button" <?php isActive("InsertEvent.php"); ?> href="InsertEvent.php" class="btn btn-secondary">Registra il tuo evento</a>
              <a type ="button" class="btn btn-secondary" href="#" data-toggle="modal" data-target="#messaggi">Invia Messaggio</a>
        <?php endif; ?>
        <a type="button" class="btn btn-secondary" href="ListMex.php">
          Messaggi <span class="badge badge-light"><?php echo $templateParams["Numero_messaggi"]; ?></span>
        </a>
        </div>
      </div>
    </div>
      <!-- Section:botton bar -->


    



    <!-- End:bottonbar -->
    <hr>
    <!--Section:Feste attive-->
    <div >
    <?php if(isset($_SESSION["Admin"])):?>
      <h2 class="card-title"><i class="fa fa-rocket fa-fw"></i>Feste approvate</h2>
      <?php endif; ?>


      <?php if(!isset($_SESSION["Admin"])):?>
          <h2 class="card-title"><i class="fa fa-rocket fa-fw"></i>Feste Attive</h2>
      <?php endif; ?>
    </div>

        <div class="row">
          <?php if(isset($templateParams["events_active"]) && (!isset($_SESSION["Admin"]))){
            require($templateParams["events_active"]);
          }
          ?>
          <?php if(isset($_SESSION["Admin"])):?>
            <?php if(isset($templateParams["EventiAdminApprovati"])){
            require($templateParams["EventiAdminApprovati"]);
          }
          ?>
          <?php endif; ?>
    </div>
    <!-- End :Feste attive -->
<hr>
     <!--Section:Feste finite-->
     <div>
      <?php if(!isset($_SESSION["Admin"])):?>
          <h2 class="card-title"><i class="fa fa-rocket fa-fw"></i>Feste Passate</h2>
      <?php endif; ?>
      <div class="row">
          <?php if(isset($templateParams["events_past"])){
            require($templateParams["events_past"]);
           
          }
          ?>
          </div>
    </div>
    <!-- End :Feste finite -->

    <hr>





    </div>
    <!--End of Container-->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; IbizaByNight</p>
      </div>
    </footer>
    <!-- END: Footer -->


    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  </body>

  
<!-- Modale: invia messaggio -->
<?php if (isset($_SESSION["PK_organizzatore"])) : ?>

<div class="modal fade" id="messaggi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Il messaggio verrà visualizzato da tutti i partecipanti dell'evento selezionato</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="<?php echo 'InsertMex.php?Organizzatore=' . $_SESSION["PK_organizzatore"]?>">
          <div class="form-group">
            <label for="message-text" class="col-form-label"> <strong> Oggetto: </strong> </label>
            <input type="text" id="NomeEvento" name="Oggetto" class="form-control" value="">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label"> <strong> Messaggio: </strong> </label>
            <textarea class="form-control" rows="4" name="Messaggio" id="message-text"></textarea>
          </div>
       
          <div class="form-group">
            <label for="sel1"><strong>Partecipanti dell'evento:</strong> </label>
            <select name="Destinatari" class="form-control" id="sel1">
              
              <?php foreach ($templateParams["array"] as $Festa) : ?>
                <option><?php echo $Festa["Nome"]; ?></option>
              <?php endforeach; ?>
            </select>

          </div>

      </div>
      <div class="modal-footer text-center">
        <input value="Chiudi" type="button" class="btn btn-lg btn-primary text-uppercase bg-dark" style="border-color: white;" data-dismiss="modal"></input>
        <input value="Invia" class="btn btn-lg btn-primary text-uppercase bg-dark" style="border-color: white;" type="submit"></input>
      </div>
    </div>
  </div>
  </form>
</div>
<?php endif; ?>
<!-- Fine modale messaggio -->
  </html>
<?php endforeach; ?>
<!DOCTYPE html>
<html lang="it">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Carrello</title>


    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">

</head>

<body style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;">

    <!-- Navigation -->
    <?php
    if(isset($templateParams["nav"])){
        require($templateParams["nav"]);
    }
    ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class="mt-4 mb-3">Carrello
            <p><small>Prodotti nel carrello:</small></p>
        </h1>



        <!-- Project One -->
        <div class="row">

            <?php
    if(isset($templateParams["EventiNelCarrello"])){
        require($templateParams["EventiNelCarrello"]);
    }
    ?>
    </div>
        </div>
        <!-- /.container -->
        <!-- Footer -->
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; IbizaByNight</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

</body>

</html>

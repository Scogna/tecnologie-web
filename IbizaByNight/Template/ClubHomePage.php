<?php foreach($templateParams["ClubCasuali"] as $ClubCasuale): ?>
<div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" class="col-lg-4 mb-4">
  <div class="card h-100">
    <?php $jpg =".jpg";?>
  <a href="<?php echo $ClubCasuale["Nome"].".php" ?>"><img href="<?php echo UPLOAD_DIR.$ClubCasuale["Nome"].".php" ?>" class="card-img-top" <?php if(isset($ClubCasuale["Nome"])): ?> src="<?php echo UPLOAD_DIR.$ClubCasuale["Nome"].$jpg; ?>" <?php endif; ?> style=" width: 100%; height: 150px" alt="Immagine evento"></a>
    <h4 class="card-header"><?php echo $ClubCasuale["Nome"]; ?></h4>
    <div class="card-body">
      <p class="card-text"><?php echo $ClubCasuale["Descrizione"]; ?></p>
    </div>
    <div class="card-footer">
      <?php if(isset($_SESSION["PK_utente"])):?>
      <a href="<?php echo $ClubCasuale["Nome"]; ?>.php"
        class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark"
        style="border-color: white;">Scegli un evento</a>
      <?php endif; ?>
      <?php if(isset($_SESSION["PK_organizzatore"])):?>
      <a href="<?php echo $ClubCasuale["Nome"]; ?>.php"
        class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark"
        style="border-color: white;">Organizza il tuo evento</a>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php endforeach; ?>
<nav style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" <?php isActive("HomePage.php");?>href="HomePage.php"> Ibiza By Night
      <?php if(isset($_SESSION["username"])): ?>
      <?php echo  " , Ciao ".$_SESSION["username"]; ?>
      <?php endif; ?></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
      data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
      aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <?php if(!isset($_SESSION["username"])):?>
        <li class="nav-item">
          <a class="nav-link" <?php isActive("Login.php");?> href="SignIn.php">Registrati</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" <?php isActive("Login.php");?> href="Login.php">Accedi</a>
        </li>
        <?php endif;?>
        <li class="nav-item">
          <a class="nav-link" <?php isActive("Clubs.php");?> href="Clubs.php">Clubs</a>
        </li>
        <?php if(isset($_SESSION["username"])):?>
        <li class="nav-item">
          <a class="nav-link" <?php isActive("AreaPrivataClient.php");?> href="AreaPrivataClient.php">Area privata</a>
        </li>
        <?php endif;?>
        <?php if(isset($_SESSION["username"]) && isset($_SESSION["PK_utente"])):?>
        <li class="nav-item">
          <a class="nav-link" <?php isActive("Cart.php");?> href="Cart.php">Carrello</a>
        </li>
        <?php endif;?>
        

        <li class="nav-item">
          <a class="nav-link" <?php isActive("Feste.php");?> href="Feste.php">Tutte le feste</a>
        </li>
        <?php if(isset($_SESSION["username"])):?>
        <li class="nav-item">
          <a class="nav-link" <?php isActive("LogOut.php");?> href="Logout.php">LogOut</a>
        </li>
        <?php endif;?>



      </ul>
    </div>
  </div>
</nav>
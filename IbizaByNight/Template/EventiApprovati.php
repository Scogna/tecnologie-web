<?php $deliteModal = "a"; ?>

<?php foreach ($templateParams["EventiApprovati"] as $Festa) : ?>
    <?php if($Festa["Approvato"] == true):?>
<?php $deliteModal++ ?>
<div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" id="cartaEventi" class="col-lg-3 mb-4">
  <div class="card h-100">
    <a><img class="card-img-top" <?php if (isset($Festa["Immagine"])) : ?> src="<?php echo UPLOAD_DIR . $Festa["Immagine"]; ?>" <?php endif; ?> style=" width: 100%; height: 150px"  alt="Immagine evento"></a>
      <h4 class="card-header"><strong><?php echo $Festa["Nome"]; ?></strong> </h4>

      <div class="card-body">
      <p class="card-text"><strong>Descrizione:</strong> <?php echo $Festa["Descrizione"]; ?></p>
      <p class="card-text"><strong>Prezzo:</strong> <?php echo $Festa["Prezzo"] . "€"; ?></p>
      <p class="card-text"><strong>Ospite:</strong> <?php echo $Festa["Ospite"]; ?></p>
      <p class="card-text"><strong>Data:</strong> <?php echo $Festa["Data"]; ?></p>
      <p class="card-text"><strong>Dove:</strong> <?php echo $Festa["Club"]; ?></p>
      <p class="card-text"><strong>Oraganizzato da:</strong> <?php echo $Festa["InseritoDa"]; ?></p>
    </div>


   
  

    <?php if ($Festa["Approvato"] == true) : ?>
    <div class="card-footer text-center">
    <a  class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-toggle="modal" style="border-color: white; color: white;" data-target="#_<?php echo $deliteModal;?>">Elimina</a></div>
    <?php endif; ?> 





<!-- Modale ELIMINA-->
<div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" id="_<?php echo $deliteModal?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ELIMINA</h4>
            </div>
            <div class="modal-body">
                <p><?php echo $_SESSION["username"]; ?> sei sicuro di voler eliminare il <?php echo $Festa["Nome"]; ?>?</p>
            </div>
            <div class="modal-footer">
            <a style="border-color: white; color: white;" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" href="<?php echo 'DeleteEvent.php?PK_evento='.$Festa["PK_evento"] ?>">Si</a>
            <button type="button"  style="border-color: white;" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<!-- Fine Modale -->

  </div>
</div>
<?php endif; ?>
<?php endforeach; ?>

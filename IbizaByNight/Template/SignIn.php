<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Registrati</title>

  <!-- Bootstrap core CSS -->
  <link href="js/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="js/modern-business.css" rel="stylesheet">
  <link href="vendor/bootstrap/css/MyCustom.css" rel="stylesheet">

</head>

<!-- Modale -->
<?php
    if(isset($templateParams["modale"])){
        require($templateParams["modale"]);
    }
    ?>
<!-- Fine Modale -->

<body style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;">

  <!-- Navigation -->
  <?php
    if(isset($templateParams["nav"])){
        require($templateParams["nav"]);
    }
    ?>

  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Registrati
      <?php if(isset($templateParams["msg"])):?>
      <p><?php echo $templateParams["msg"]; 
              ?><p>
          <?php endif; ?>
    </h1>



    <!-- Marketing Icons Section -->
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-xl-9 mx-auto">
          <div class="card card-signin flex-row my-5">
            <div class="card-body">
              <h5 class="card-title text-center">Inserisci i tuoi dati</h5>
              <form action="SignIn.php" enctype="multipart/form-data" method="POST">

                <div class="form-label-group">
                <label class="label-cust" for="Nome">Nome</label>
                  <input type="text" id="Nome" name="Nome" class="form-control" placeholder="Nome" required autofocus />
                </div>
                <div class="form-label-group">
                <label class="label-cust" for="Cognome">Cognome</label>
                  <input type="text" id="Cognome" name="Cognome" class="form-control" placeholder="Cognome" required autofocus />
                 
                </div>
                <div class="form-label-group">
                <label class="label-cust" for="Userame">Username</label>
                  <input type="text" id="Userame" name="Username" class="form-control" placeholder="Username" required  autofocus />
                  
                </div>

                <div class="form-label-group">
                <label class="label-cust" for="Email">Email address</label>
                  <input type="email" id="Email" name="Email" class="form-control" placeholder="Email address" required />
                  
                </div>

                  
                  <div class="form-label-group">
                  <label class="label-cust"  for="ImgProfilo">Immagine Profilo</label>
                    <input type="file" name="ImgProfilo" id="ImgProfilo" class="form-control" accept="image/*" />
                    
                  </div>

                  <hr>

                  <div class="form-label-group">
                  <label class="label-cust"  for="Password">Password</label>
                    <input type="Password" name="Password" id="Password" class="form-control" placeholder="Password"
                      required />
                    
                  </div>

                  <div class="form-label-group">
                  <label class="label-cust" for="ConfermaPassword">Conferma Password</label>
                    <input type="Password" name="ConfermaPassword" id="ConfermaPassword" class="form-control" placeholder="ConfermaPassword"
                      required />
                   
                  </div>
                    <hr>

                  <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" name="check" class="custom-control-input" vulue="si" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">Sono un organizzatore</label>
                  </div>
                

                <input  id="SignIn"class="btn btn-lg btn-primary btn-block text-uppercase bg-dark" style="border-color: white;"
                  name="submit" value="Registrati" type="submit"></input>

                
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; IbizaByNight</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>

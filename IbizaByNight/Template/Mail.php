<?php $infomodal = 0; ?>
<link href="vendor/bootstrap/css/MyCustom.css" rel="stylesheet">
<?php foreach ($templateParams["array1"] as $Mex) : ?>

    <?php $infomodal++; ?>
    

    <div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" class="mail-box">
        <aside class="lg-side">
            <div class="inbox-body">
                <table class="table table-inbox table-hover">
                    <tbody>
                        <tr class="unread">
                            <!-- Se è presente-->
                            <?php if (!empty($templateParams["array_comm_visti"])) : ?>
                                <?php if (in_array($Mex["comment_id"], $templateParams["array_comm_visti"])) : ?>
                                    <td class="table td"><?php echo $Mex["comment_data"]; ?></td>
                                    <td class="table-object"><?php echo $Mex["comment_object"]; ?></td>
                                    <td class="table td"> <?php echo $Mex["comment_subject"]; ?></td>
                                    <!-- <td class="view-message  inbox-small-cells"><i class="fa fa-paperclip"></i></td> -->
                                    <td class="table-right" data-toggle="modal" data-target="#_<?php echo $infomodal; ?>">
                                        <a class="a-mail" href="#">
                                            Read
                                        </a>
                                    </td>
                                <?php endif; ?>
                            <?php endif; ?>
                            <!-- Altrimenti-->
                            <?php if ((empty($templateParams["array_comm_visti"])) || !(in_array($Mex["comment_id"], $templateParams["array_comm_visti"]))) : ?>
                                <td class="table td "><strong><?php echo $Mex["comment_data"]; ?></strong></td>
                                <td class="table-object"><strong><?php echo $Mex["comment_object"]; ?></strong></td>
                                <td class="table td "> <strong> <?php echo $Mex["comment_subject"]; ?></strong></td>
                                <!-- <td class="view-message  inbox-small-cells"><i class="fa fa-paperclip"></i></td> -->
                                <td class="table-right " data-toggle="modal" data-target="#_<?php echo $infomodal; ?>">
                                    <a class="a-mail" href="#">
                                        Read
                                    </a>
                                </td>
                            <?php endif; ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </aside>
    </div>


    <div class="modal fade" id="_<?php echo $infomodal; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <!-- Modale -->

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Messaggio:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo 'ListMex.php?Id=' . $Mex["comment_id"] ?>">
                        <div class="form-group">

                            <textarea class="form-control" readonly class="form-control-plaintext" rows="6" name="Descrizione" id="message-text"> <?php echo $Mex["comment_text"]; ?></textarea>
                        </div>

                </div>
                <div class="modal-footer text-center">
                    <input value="Visto" class="btn btn-lg btn-primary text-uppercase bg-dark" style="border-color: white;" type="submit"></input>
                </div>
            </div>
        </div>
        </form>


    </div>






<?php endforeach; ?>
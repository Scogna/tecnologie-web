<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Password dimenticata</title>

  <!-- Bootstrap core CSS -->
  <link href="js/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="js/modern-business.css" rel="stylesheet">

</head>



<body style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;">

  <!-- Navigation -->
  <?php
    if(isset($templateParams["nav"])){
        require($templateParams["nav"]);
    }
    ?>

  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Password dimenticata
      <p><small>Per recuparare la tua password abbiamo bisogno di qualche informazione</small></p>
      <?php if(isset($templateParams["msg"])):?> <p><?php echo  $templateParams["msg"]; ?><p> <?php endif; ?>
    </h1>



    <!-- Marketing Icons Section -->
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-xl-9 mx-auto">
          <div class="card card-signin flex-row my-5">
            <div class="card-img-left d-none d-md-flex">
              <!-- Background image for card set in CSS! -->
            </div>
            <div class="card-body">
              <h5 class="card-title text-center">Inserisci i tuoi dati</h5>
              <form action="PasswordDimenticata.php" method="POST">

                <div class="form-label-group">
                  <input type="text" id="Userame" name="Username" class="form-control" placeholder="Username" required
                    autofocus />
                  <label for="Userame">Username</label>
                </div>

                <div class="form-label-group">
                  <input type="email" id="Email" name="Email" class="form-control" placeholder="Email address"
                    required />
                  <label for="Email">Email address</label>
                </div>

                <div class="form-label-group">
                  <input type="text" id="Email" name="CodiceSicurezza" class="form-control"
                    placeholder="CodiceSicurezza" required />
                  <label for="CodiceSicurezza">Codice Sicurezza</label>
                </div>
                <hr>

                <div class="custom-control custom-checkbox mb-3">
                  <input type="checkbox" name="check" class="custom-control-input" vulue="si" id="customCheck1">
                  <label class="custom-control-label" for="customCheck1">Sono un organizzatore</label>
                </div>

                <div class="form-label-group">
                  <input type="Password" id="Password" name="Password" class="form-control" placeholder="Nuova Password"
                    required />
                  <label for="Password">Nuova Password</label>
                </div>

                <div class="form-label-group">
                  <input type="Password" id="Password" name="ConfermaPassword" class="form-control"
                    placeholder="Nuova Password" required />

                  <label for="Password">Conferma nuova Password</label>
                </div>




                <input id="PasswordDimenticata" class="btn btn-lg btn-primary btn-block text-uppercase bg-dark"
                  style="border-color: white;" name="submit" value="Cambia Password" type="submit"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <p class="m-0 text-center text-white">Copyright &copy; IbizaByNight</p>

    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>

</html>

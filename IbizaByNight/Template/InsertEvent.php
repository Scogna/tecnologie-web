<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Inserisci il tuo evento</title>

  <!-- Bootstrap core CSS -->
  <link href="js/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="vendor/bootstrap/css/MyCustom.css" rel="stylesheet">


</head>


<body>

  <!-- Navigation -->
  <?php
    if(isset($templateParams["nav"])){
        require($templateParams["nav"]);
    }
    ?>

  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Inserisci l'evento </h1>



    <!-- Marketing Icons Section -->
    <div class="container">
      <div class="row">
        <div class="col-lg-10 col-xl-9 mx-auto">
          <div class="card card-signin flex-row my-5">
            <div class="card-body">
              <h5 class="card-title text-center">Inserisci i dati dell'evento</h5>
              <form action="InsertEvent.php" enctype="multipart/form-data" method="POST">

                <div class="form-label-group">
                <label  class="label-cust" for="NomeEvento">Nome Evento</label>
                  <input type="text" id="NomeEvento" name="NomeEvento" class="form-control" placeholder="NomeEvento"
                    required autofocus>
                 
                </div>

                <div class="form-group">
                <label  class="label-cust" for="Descrizione">Descrizione</label>
                  <textarea class="form-control" name="Descrizione" placeholder="Descrizione"></textarea>
                </div>

                <div class="form-label-group">
                <label  class="label-cust" for="Prezzo">Prezzo biglietto</label>
                  <input type="number" id="Prezzo" name="Prezzo" class="form-control" placeholder="Prezzo biglietto"
                    required autofocus>
                </div>

                <div class="form-label-group">
                <label  class="label-cust" for="Partecipanti">Numero Partecipanti</label>
                  <input type="number" id="Partecipanti" name="Partecipanti" class="form-control"
                    placeholder="Numero Partecipanti" required autofocus>
                </div>

                <div class="form-label-group">
                <label  class="label-cust" for="Ospite">Ospite</label>
                  <input type="text" id="Ospite" name="Ospite" class="form-control" placeholder="Ospite" required
                    autofocus>
                </div>


                <div class="form-label-group">
                <label  class="label-cust" for="Data">Data</label>
                  <input type="date" id="Data" name="data" class="form-control" placeholder="Data" required>
                </div>

                <div class="form-label-group">
                <label  class="label-cust" for="imgEvento">Immagine Evento</label>
                  <input type="file" name="imgEvento" id="imgEvento" class="form-control" accept="image/*" />
                </div>


                <div class="form-group">
                <label  class="label-cust" for="sel1">Clubs:</label>
                  <select name="club" class="form-control" id="sel1">
                    <?php foreach($templateParams["clubs"] as $club): ?>
                    <option><?php echo $club["Nome"]; ?></option>
                    <?php endforeach; ?>
                  </select>
                  
                </div>

                <input class="btn btn-lg btn-primary btn-block text-uppercase bg-dark" style="border-color: white;"
                  name="submit" value="Registra il tuo evento" type="submit"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; IbizaByNight</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>

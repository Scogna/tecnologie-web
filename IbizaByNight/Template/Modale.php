<!-- Modale NON DISPONIBILITA-->
<div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" id="NonDisponibile" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NON DISPONIBILE</h4>
            </div>
            <div class="modal-body">
                <p>Siamo spiacenti ma l'evento da lei selezionato non è piu disponibile.</p>
            </div>
            <div class="modal-footer">
            <button type="button"  style="border-color: white;" class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>
<!-- Fine Modale -->




<!DOCTYPE html>
<html lang="en">
<style>
  .filterDiv {
    display: none;
    padding-right: 15px;
    padding-left: 15px;
    margin-bottom: 1.5rem !important;
    position: relative;
    width: 100%;
  }

  .show {
    display: inline-block;

  }
</style>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Tutti gli eventi</title>

  <!-- Bootstrap core CSS -->
  <link href="js/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="js/modern-business.css" rel="stylesheet">

</head>

<body style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;">

  <!-- Navigation -->
  <?php
    if(isset($templateParams["nav"])){
        require($templateParams["nav"]);
    }
    ?>

  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Trova il tuo evento
      <hr>
      <p><small>Tutti gli eventi</small></p>
    </h1>
    <div class="btn-group dropright">
  <button  id="val" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
  </button>
  <div class="dropdown-menu">
  <a class="dropdown-item" onclick="filterSelectionMesi('Tutti i mesi')"> Tutti i mesi</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Gennaio')"> Gennaio</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Febbraio')"> Febbraio</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Marzo')"> Marzo</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Aprile')"> Aprile</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Maggio')"> Maggio</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Giugno')"> Giugno</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Luglio')"> Luglio</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Agosto')"> Agosto</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Settembre')"> Settembre</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Ottobre')"> Ottobre</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Novembre')"> Novembre</a>
    <a class="dropdown-item" onclick="filterSelectionMesi('Dicembre')"> Dicembre</a>
  </div>
  
</div>
<div class="btn-group dropright">
  <button id="val1" value="Tutti i clubs" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    
  </button>
  <div class="dropdown-menu">
  <a class="dropdown-item" onclick="filterSelectionClub('Tutti Clubs')"> Tutte</a>
    <a class="dropdown-item" onclick="filterSelectionClub('DC10')"> DC10</a>
    <a class="dropdown-item" onclick="filterSelectionClub('Pacha')"> Pacha</a>
    <a class="dropdown-item" onclick="filterSelectionClub('Space')"> Space</a>
    <a class="dropdown-item" onclick="filterSelectionClub('Amnesia')"> Amnesia</a>
    <a class="dropdown-item" onclick="filterSelectionClub('BoatParty')"> BoatParty</a>
    <a class="dropdown-item" onclick="filterSelectionClub('FesteInVilla')"> FesteInVilla</a>
  </div>
  
</div>
    <hr>
    <h2 class="mt-4 mb-3" id="Mesi"></h2> 
    <div class="row">

    

      <?php
    if(isset($templateParams["Evento"])){
        require($templateParams["Evento"]);
    }
    ?>
    </div>
    <script>
      filterSelectionClub("Tutti Clubs")
      filterSelectionMesi("Tutti i mesi")

      function filterSelectionClub(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        document.getElementById("val1").innerHTML = c;
        document.getElementById("Mesi").innerHTML = c;
        if (c == "Tutti Clubs") c = "";
        for (i = 0; i < x.length; i++) {
          w3RemoveClass(x[i], "show");
          if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
      }

      function filterSelectionMesi(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        document.getElementById("val").innerHTML = c;
        document.getElementById("Mesi").innerHTML =  c;
        if (c == "Tutti i mesi") c = "";
        for (i = 0; i < x.length; i++) {
          w3RemoveClass(x[i], "show");
          if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
        }
      }

      function w3AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
          if (arr1.indexOf(arr2[i]) == -1) {
            element.className += " " + arr2[i];
          }
        }
      }

      function w3RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
          while (arr1.indexOf(arr2[i]) > -1) {
            arr1.splice(arr1.indexOf(arr2[i]), 1);
          }
        }
        element.className = arr1.join(" ");
      }

      // Add active class to the current a (highlight it)
      var btnContainer = document.getElementById("myBtnContainer");
      var btns = btnContainer.getElementsByClassName("btn");
      for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function () {
          var current = document.getElementsByClassName("active");
          current[0].className = current[0].className.replace(" active", "");
          this.className += " active";
        });
      }
    </script>
 <hr>
  </div>
  <!-- /.container -->
 


  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <p class="m-0 text-center text-white">Copyright &copy; IbizaByNight</p>
  </footer>
  <!-- Footer -->

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>

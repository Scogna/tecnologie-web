<?php foreach ($templateParams["array"] as $Festa) : ?>
        <?php if(GetDay($Festa["Data"]) == false): ?>
    <div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" class="col-lg-3 col-md-4 col-sm-6 ">
    <div class="card h-100">
      <a><img class="card-img-top" <?php if (isset($Festa["Immagine"])) : ?> src="<?php echo UPLOAD_DIR . $Festa["Immagine"]; ?>" <?php endif; ?> style=" width: 100%; height: 150px" alt="Immagine evento"></a>
      <div class="card-body">
        <h4 class="card-title"><strong><?php echo $Festa["Nome"]; ?></strong> </h4>
        <p class="card-text"><strong>Descrizione:</strong> <?php echo $Festa["Descrizione"]; ?></p>
        <p class="card-text"><strong>Prezzo:</strong> <?php echo $Festa["Prezzo"] . "€"; ?></p>
        <p class="card-text"><strong>Ospite:</strong> <?php echo $Festa["Ospite"]; ?></p>
        <p class="card-text"><strong>Data:</strong> <?php echo $Festa["Data"]; ?></p>
        <p class="card-text"><strong>Dove:</strong> <?php echo $Festa["Club"]; ?></p>
        <p class="card-text"><strong>Oraganizzato da:</strong> <?php echo $Festa["InseritoDa"]; ?></p>

      </div>
      
    </div>
  </div>
  <?php endif; ?>
<?php endforeach; ?>

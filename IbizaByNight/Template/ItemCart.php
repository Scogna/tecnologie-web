<?php $confirmModal =0; 
      $deliteModal ="a";?>
<?php foreach($templateParams["EventiCarrello"] as $Evento): ?>
  <?php $confirmModal++;
        $deliteModal++; ?>
<hr>
<div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;"  class="col-md-7">
  <a> <img class="img-fluid rounded mb-3 mb-md-0" <?php if(isset($Evento["Immagine"])): ?>   src="<?php echo UPLOAD_DIR.$Evento["Immagine"]; ?>" <?php endif; ?> style="height: 440px" alt="">  </a>
</div>
<div  style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" class="col-md-5">
  <h3><?php echo $Evento["Nome"]; ?></h3>
  <p class="card-text"><strong>Descrizione:</strong> <?php echo $Evento["Descrizione"]; ?></p>
  <p class="card-text"><strong>Prezzo:</strong> <?php echo $Evento["Prezzo"]." €"; ?></p>
  <p id="x_<?php echo $confirmModal;?>" style="display: none"> <?php echo $Evento["Prezzo"]; ?></p>
  <p class="card-text"><strong>Posti disponibili:</strong> <?php echo $Evento["Partecipanti"]; ?></p>
  <p class="card-text"><strong>Ospite:</strong> <?php echo $Evento["Ospite"]; ?></p>
  <p class="card-text"><strong>Data:</strong> <?php echo $Evento["Data"]; ?></p>
  <p class="card-text"><strong>Dove:</strong> <?php echo $Evento["Club"]; ?></p>
  <p class="card-text"><strong>Oraganizzato da:</strong> <?php echo $Evento["InseritoDa"]; ?></p>
  <div class="row" style=" padding: inherit;">
    <label style="margin-right:2%" for="s_<?php echo $confirmModal;?>"><strong>Quantit&agrave:</strong></label>
    <?php $totale = "z_".$confirmModal;
    $prezzo = "x_".$confirmModal; 
    $quantita = "s_".$confirmModal;?>

<form method="POST"
      action="<?php echo 'Confirm.php?Evento='.$Evento["PK_evento"].'&Data='.$Evento["Data"].'&Posti='.$Evento["Partecipanti"] ?>"
      name="modulo">


        
      <select name="quantita" id="s_<?php echo $confirmModal;?>" onchange="myFunction(<?php echo $prezzo;?>,<?php echo $quantita;?>,<?php echo $totale;?>)">
        <?php if($Evento["Partecipanti"] >= 1):?>
        <option value="1" >1</option>
        <?php endif;?>
        <?php if($Evento["Partecipanti"] >= 2):?>
        <option value="2">2</option>
        <?php endif;?>
        <?php if($Evento["Partecipanti"] >= 3):?>
        <option value="3">3</option>
        <?php endif;?>
        <?php if($Evento["Partecipanti"] >= 4):?>
        <option value="4">4</option>
        <?php endif;?>
      </select> 
  </div>
  <p class="card-text" id="z_<?php echo $confirmModal;?>"> <strong>Totale: </strong> <?php echo $Evento["Prezzo"]." €"; ?></p>

        <script>

            function myFunction(prezzo,qnt,totale){
                var e = qnt.value;
                var x = prezzo.innerHTML;
				totale.innerHTML = "<strong>Totale: </strong>" + e * x + "€";
			}
        </script>
  <a class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-toggle="modal" style="border-color: white; color: white;" data-target="#_<?php echo $confirmModal;?>">Conferma</a>
  <a type="submit" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-toggle="modal" style="border-color: white; color: white;" data-target="#_<?php echo $deliteModal;?>">Elimina</a>
  
  <hr>
</div>


<!-- Modale CONFERMA-->
<div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" id="_<?php echo $confirmModal?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">CONFERMA
                  <small><p>Complimenti <?php echo $_SESSION["username"]; ?>, stai acquistando <?php echo $Evento["Nome"]; ?>.</p></small>
                </h4>
            </div>
            <div class="modal-body">
                <p>Premi ok per finalizzare l'acquisto!</p>
            </div>
            <div class="modal-footer">
            <input style="border-color: white;" value="ok" type="submit" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark"></input>
            </div>
        </div>
    </div>
</div>
<!-- Fine Modale -->


<!-- Modale ELIMINA-->
<div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" id="_<?php echo $deliteModal?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ELIMINA</h4>
            </div>
            <div class="modal-body">
                <p><?php echo $_SESSION["username"]; ?> sei sicuro di voler eliminare l'evento <?php echo $Evento["Nome"]; ?> dal carrello?</p>
            </div>
            <div class="modal-footer">
            <a style="border-color: white; color: white;" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" href="<?php echo 'DeleteItem.php?Evento='.$Evento["PK_evento"] ?>">Si</a>
            <button type="button"  style="border-color: white;" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<!-- Fine Modale -->

</form>

<?php endforeach; ?>
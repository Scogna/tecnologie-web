<!DOCTYPE html>
<html lang="it">

<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Ibiza by Night</title>


  <!-- Bootstrap core CSS -->
  <link href="js/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
  <link href="vendor/bootstrap/css/MyCustom.css" rel="stylesheet">

</head>

<body  style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;">

  
<!-- Navigation -->

<div>
  <?php
    if(isset($templateParams["nav"])){
        require($templateParams["nav"]);
    }
    ?>
    </div>
  <!-- Navigation -->
  <div class="container">
  <!-- IMMAGINI -->
  <header class="header">
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" >
          <img src="upload/elRow.jpg" alt="">
          <div class="carousel-caption d-none d-md-block">
            <h3>Amnesia</h3>
            <p>Senza dubbio la quintessenza delle esperienze clubbing di Ibiza.</p>
          </div>
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below -->
        <div class="carousel-item"  >
        <img src="upload/afterlife.jpg" alt="">
          <div class="carousel-caption d-none d-md-block">
            <h3>Space</h3>
            <p>L'ultima aggiunta al paradiso delle meraviglie di Playa d'en Bossa.</p>
          </div>
        </div>
        <!-- Slide Three - Set the background image for this slide in the line below -->
        <div class="carousel-item" >
        <img src="upload/paradise.jpg" alt="">
          <div class="carousel-caption d-none d-md-block">
            <h3>DC10</h3>
            <p>Per essere famoso è famoso, quanto tutto ciò che c'è sull'isola.</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </header>
  <!-- IMMAGINI -->
  
  <!-- IMMAGINI -->
 <!-- Page ORG o Utente --> 
 <?php if(!isset($_SESSION["Admin"])):?> 
  

    <h1 class="my-4">Benvenuto<?php if(isset($_SESSION["username"])): ?>
      <?php echo $_SESSION["username"].","; ?>
      <?php endif; ?>
       <?php if(isset($_SESSION["PK_utente"]) || !isset($_SESSION["PK_organizzatore"])): ?> ecco per te i migliori clubs di Ibiza.<?php endif; ?>
        <?php if(isset($_SESSION["PK_organizzatore"])): ?>questi sono alcuni dei club in cui potrai organizzare il tuo evento.<?php endif; ?> 

    </h1>

    <!-- Clubs -->
    <div class="row">
      <?php
    if(isset($templateParams["Clubs"])){
        require($templateParams["Clubs"]);
    }
    ?>

    </div>
    <!-- Clubs -->
 <hr>
    <!-- FESTE -->
    <h2>
    <?php if(isset($_SESSION["PK_utente"]) || !isset($_SESSION["PK_organizzatore"])): ?> Eventi scelti per te.<?php endif; ?>
      <?php if(isset($_SESSION["PK_organizzatore"])): ?> Qui verranno mostrati i tuoi eventi insieme ad altri.<?php endif; ?> 

    </h2>

    <div class="row">
      <?php
    if(isset($templateParams["Evento"])){
        require($templateParams["Evento"]);
    }
    ?>
    </div>
    <!-- FESTE -->

 <hr>
  

  <!-- Call to Action Section -->
  <?php endif; ?> 
  <!-- End: Org o Utente --> 
 
 
 <!-- Page Admin --> 
 <?php if(isset($_SESSION["Admin"])):?> 
  <div class="container"> 
 
    <h1 class="my-4">Benvenuto<?php if(isset($_SESSION["username"])): ?> 
      <?php echo $_SESSION["username"].","; ?> 
      <?php endif; ?> 
       Aggiungi un nuovo Club. 
             
    </h1> 
 
    <!-- Clubs --> 
     <!-- Marketing Icons Section -->  
      <div class="row"> 
        <div class="col-lg-10 col-xl-9 mx-auto"> 
          <div class="card card-signin flex-row my-5"> 
            <div class="card-body"> 
              <h5 class="card-title text-center">Inserisci i dati del club</h5> 
              <form action="HomePage.php" enctype="multipart/form-data" method="POST"> 
 
                <div class="form-label-group"> 
                  <input type="text" id="NomeClub" name="NomeClub" class="form-control" placeholder="NomeClub" 
                    required autofocus> 
                  <label for="NomeClub">Nome club</label> 
                </div> 
 
                <div class="form-group"> 
                  <textarea class="form-control" name="DescrizioneHomePage" placeholder="DescrizioneHomePage"></textarea> 
                  <label for="DescrizioneHomePage">Descrizione HomePage</label> 
                </div> 
 
                <div class="form-group"> 
                  <textarea class="form-control" name="DescrizioneDettagliata" placeholder="DescrizioneDettagliata"></textarea> 
                  <label for="DescrizioneDettagliata">Descrizione dettagliata</label> 
                </div> 
 
                 
                <div class="form-group"> 
                  <textarea class="form-control" name="FamosaPer" placeholder="FamosaPer"></textarea> 
                  <label for="FamosaPer">Famosa per</label> 
                </div> 
                  
                <div class="form-group"> 
                  <textarea class="form-control" name="FesteFamose" placeholder="FesteFamose"></textarea> 
                  <label for="FesteFamose">Feste Famose</label> 
                </div> 
 
                <div class="form-group"> 
                  <textarea class="form-control" name="ComeArrivare" placeholder="ComeArrivare"></textarea> 
                  <label for="ComeArrivare">Come arrivare</label> 
                </div> 
  
 
                <div class="form-label-group"> 
                  <input type="file" name="imgClub" id="imgClub" class="form-control" accept="image/*" /> 
                  <p> <label for="imgClub">Immagine Club</label></p> 
                </div> 
 
                <input class="btn btn-lg btn-primary btn-block text-uppercase bg-dark" style="border-color: white;" 
                  name="submit" value="Registra Club" type="submit"></input> 
              </form> 
            </div> 
          </div> 
        </div> 
      </div> 
    </div> 
    <!-- /.row --> 
 
    <!-- Clubs --> 
 <hr> 
    <!-- FESTE --> 
    <h2> 
    Eventi da approvare. 
 
    </h2> 
 
    <div class="row"> 
      <?php 
    if(isset($templateParams["EventoAdmin"])){ 
        require($templateParams["EventoAdmin"]); 
    } 
    ?> 
    </div> 
    <!-- FESTE --> 
 
 <hr> 
  
  <?php endif; ?> 
  <!-- /.container -->
  </div>
  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; IbizaByNight</p>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


</body>

</html>
<?php $infomodal =0;
    $deliteModal = "a" ?>
<?php foreach ($templateParams["Clubs"] as $Club) : ?>
  <?php $infomodal++;
      $deliteModal++; ?>


    <div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" class="col-lg-6 portfolio-item">
        <div class="card h-100">
          <a  href="<?php echo $Club["Nome"].".php" ?>" ><img class="card-img-top" <?php if (isset($Club["Immagine"])) : ?>
              src="<?php echo UPLOAD_DIR . $Club["Immagine"]; ?>" <?php endif; ?> alt="Immagine Club"></a>
          <div class="card-body">
            <h4 class="card-title">
              <a href=<?php echo $Club["Nome"].".php"?>><?php echo $Club["Nome"]?></a>
            </h4>
            <p class="card-text"><?php echo $Club["DescrDett"]?></p>
            <p class="card-text"> <strong>Famosa per:</strong> <?php echo $Club["FamosaPer"]?></p>
            <p class="card-text"> <strong>Feste famose:</strong> <?php echo $Club["FesteFamose"]?></p>
            <p class="card-text"><strong>Come arrivare:</strong> <?php echo $Club["ComeArrivare"]?></p>

           <?php if(isset($_SESSION["Admin"])):?>
            <div class="card-footer text-center">
            <a class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-toggle="modal" style="border-color: white; color: white;" data-target="#_<?php echo $infomodal;?>">Modifica</a>
            <a  class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-toggle="modal" style="border-color: white; color: white;" data-target="#_<?php echo $deliteModal;?>">Elimina</a>
        </div>
            <?php endif; ?> 

          </div>
        </div>
      </div>




      <div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" class="modal fade" id="_<?php echo $infomodal?>" tabindex="-1" role="dialog"
      aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modifica <?php echo $Club["Nome"] ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="POST" action="<?php echo 'ModifyClub.php?PK_club='.$Club["PK_club"]?>">
              <div class="form-group">
                <label for="message-text" class="col-form-label"> <strong> Nome Club: </strong> </label>
                <input type="text"  name="NomeClub" class="form-control"
                  value="<?php echo $Club["Nome"] ?>">
              </div>
              <div class="form-group">
                <label for="message-text" class="col-form-label"> <strong> Descrizione: </strong> </label>
                <textarea class="form-control" rows="4" name="DescrDett"
                  id="message-text"> <?php echo $Club["DescrDett"]; ?></textarea>
              </div>
              <div class="form-group">
                <label for="message-text" class="col-form-label"> <strong> Famosa Per: </strong> </label>
                <textarea class="form-control" rows="4" name="FamosaPer"
                  id="message-text"> <?php echo $Club["FamosaPer"]; ?></textarea>
              </div>
              <div class="form-group">
                <label for="message-text" class="col-form-label"> <strong> Feste Famose: </strong> </label>
                <textarea class="form-control" rows="4" name="FesteFamose"
                  id="message-text"> <?php echo $Club["FesteFamose"]; ?></textarea>
              </div>
              <div class="form-group">
                <label for="message-text" class="col-form-label"> <strong> Come Arrivare: </strong> </label>
                <textarea class="form-control" rows="4" name="ComeArrivare"
                  id="message-text"> <?php echo $Club["ComeArrivare"]; ?></textarea>
              </div>

          </div>
          <div class="modal-footer text-center">
            <input value="Chiudi" type="button" class="btn btn-lg btn-primary text-uppercase bg-dark"
              style="border-color: white;" data-dismiss="modal"></input>
            <input value="Modifica" class="btn btn-lg btn-primary text-uppercase bg-dark"
              style="border-color: white;" type="submit"></input>
          </div>
        </div>
      </div>
      </form>
    </div>

<!-- Modale ELIMINA-->
<div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" id="_<?php echo $deliteModal?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ELIMINA</h4>
            </div>
            <div class="modal-body">
                <p><?php echo $_SESSION["username"]; ?> sei sicuro di voler eliminare il <?php echo $Club["Nome"]; ?>?</p>
            </div>
            <div class="modal-footer">
            <a style="border-color: white; color: white;" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" href="<?php echo 'DeleteClub.php?PK_club='.$Club["PK_club"] ?>">Si</a>
            <button type="button"  style="border-color: white;" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<!-- Fine Modale -->


    <?php endforeach; ?>    
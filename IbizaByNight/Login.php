<?php
require_once 'connection.php';

if(isset($_POST["Username"]) && isset($_POST["Password"])){
    $username = $_POST['Username'];
    $password = $_POST['Password'];
    $login_user_result = $dbh->checkUser($username, $password);
    $login_org_result = $dbh->checkOrg($username, $password);

    if(count($login_user_result) == 0){
        if(count($login_org_result) == 0){
             //Login fallito
             $templateParams["errorelogin"] = "Errore! Controllare username o password!";
             session_destroy();
        }else{
            registerOrg($login_org_result[0]);
            loginOrg($username, $password, $dbh->getmyDB());
            header("location: HomePage.php");
        }

    }else{
        registerClient($login_user_result[0]);
        login($username, $password, $dbh->getmyDB());
        header("location: HomePage.php");
    }
    
}



$templateParams["nav"] = "nav.php";


require 'Template/Login.php';
?>
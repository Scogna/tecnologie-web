
<?php  $deliteModal = "a"; ?>

<?php foreach($templateParams["Eventi"] as $Evento): ?>
  <?php if($Evento["Approvato"] == false):?>
  <?php $approvato= true;
         $deliteModal++ ;
        $mese = getMouth($Evento["Data"]);
        $classe ="filterDiv col-lg-3 mb-4";
        $classe.= $mese;
        $_SESSION["mese"] = getMouth($Evento["Data"]);
           ?>
<div class= "<?php echo $classe?>">
  <div class="card h-100">
  <a><img class="card-img-top" <?php if(isset($Evento["Immagine"])): ?> src="<?php echo UPLOAD_DIR.$Evento["Immagine"]; ?>" <?php endif; ?> style=" width: 100%; height: 150px" alt="Immagine evento"></a>
    <h4 class="card-header"><strong><?php echo $Evento["Nome"]; ?></strong> </h4>
    <div class="card-body">
      
      <p class="card-text"><strong>Descrizione:</strong> <?php echo $Evento["Descrizione"]; ?></p>
      <p class="card-text"><strong>Prezzo:</strong> <?php echo $Evento["Prezzo"]."€"; ?></p>
      <p class="card-text"><strong>Posti disponibili:</strong> <?php echo $Evento["Partecipanti"]; ?></p>
      <p class="card-text"><strong>Ospite:</strong> <?php echo $Evento["Ospite"]; ?></p>
      <p class="card-text"><strong>Data:</strong> <?php echo $Evento["Data"]; ?></p>
      <p class="card-text"><strong>Dove:</strong> <?php echo $Evento["Club"]; ?></p>
      <p class="card-text"><strong>Oraganizzato da:</strong> <?php echo $Evento["InseritoDa"]; ?></p>
      </div>
      
      <?php if ($Evento["Approvato"] == false) : ?>
      <div class="card-footer text-center">
        <a href="<?php echo 'HomePage.php?Approvato='.$approvato.'&Evento='.$Evento["PK_evento"]?>" class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" style="border-color: white; color: white;">Approva</a>
        <?php endif; ?>
        <a  class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-toggle="modal" style="border-color: white; color: white;" data-target="#_<?php echo $deliteModal;?>">Elimina</a></div>

    </div>
</div>

<!-- Modale ELIMINA-->
<div id="_<?php echo $deliteModal?>" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">ELIMINA</h4>
            </div>
            <div class="modal-body">
                <p><?php echo $_SESSION["username"]; ?> sei sicuro di voler eliminare il <?php echo $Evento["Nome"]; ?>?</p>
            </div>
            <div class="modal-footer">
            <a style="border-color: white; color: white;" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" href="<?php echo 'DeleteEvent.php?PK_evento='.$Evento["PK_evento"] ?>">Si</a>
            <button type="button"  style="border-color: white;" class="btn btn-lg btn-primary  btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<!-- Fine Modale -->

<?php endif; ?>


<?php endforeach; ?>
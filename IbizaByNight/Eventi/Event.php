<?php $confirmModal =0;?>
<?php foreach($templateParams["Eventi"] as $Evento): ?>
<?php $approvato= true;
        $confirmModal++;
        $dove = $Evento["Club"];
        $org = $Evento["InseritoDa"];
        $mese = getMouth($Evento["Data"]);
        $classe ="filterDiv col-lg-3 mb-4";
        $classe.= $mese;
        $classe.= $dove;
           ?>
<div class="<?php echo $classe?>">
  <div class="card h-100">
    <a><img class="card-img-top" <?php if(isset($Evento["Immagine"])): ?>
        src="<?php echo UPLOAD_DIR.$Evento["Immagine"]; ?>" <?php endif; ?> style=" width: 100%; height: 150px"
        alt="Immagine evento"></a>
    <h4 class="card-header"><strong><?php echo $Evento["Nome"]; ?></strong> </h4>
    <div class="card-body">

      <p class="card-text"><strong>Descrizione:</strong> <?php echo $Evento["Descrizione"]; ?></p>
      <p class="card-text"><strong>Prezzo:</strong> <?php echo $Evento["Prezzo"]."€"; ?></p>
      <p class="card-text"><strong>Posti disponibili:</strong> <?php echo $Evento["Partecipanti"]; ?></p>
      <p class="card-text"><strong>Ospite:</strong> <?php echo $Evento["Ospite"]; ?></p>
      <p class="card-text"><strong>Data:</strong> <?php echo $Evento["Data"]; ?></p>
      <p class="card-text"><strong>Dove:</strong> <?php echo $Evento["Club"]; ?></p>
      <p class="card-text"><strong>Oraganizzato da:</strong> <?php echo $Evento["InseritoDa"]; ?></p>
    </div>


    <?php if(!isset($_SESSION["PK_organizzatore"])): ?>

    <?php if((getDay($Evento["Data"]) == true) && ($Evento["Partecipanti"] != 0)): ?>
    <div class="card-footer">
    
    <?php if(isset($_SESSION["username"]) && ($Evento["Approvato"] == true)): ?>
    <a class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-toggle="modal" style="border-color: white; color: white;" data-target="#_<?php echo $confirmModal;?>">Partecipa</a>
    <?php endif; ?>

    <?php if(!isset($_SESSION["username"])): ?>
    <a class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" href="Login.php" style="border-color: white; color: white;" >Partecipa</a>
    <?php endif; ?>
  </div>


    <?php endif; ?>


    <!-- Modale CONFERMA-->
    <div id="_<?php echo $confirmModal?>" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Aggiungi al carrello
              <small>
                <p><?php echo $_SESSION["username"]; ?>, stai acquistando <?php echo $Evento["Nome"]; ?>.
                </p>
              </small>
            </h4>
          </div>
          <div class="modal-body">
            <p>Premere ok per aggiungerlo al carrello!</p>
          </div>
          <div class="modal-footer">
          <a href="<?php echo 'Cart.php?Evento='.$Evento["PK_evento"]?>"
        class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark"
        style="border-color: white;">ok</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Fine Modale -->

    <?php if((getDay($Evento["Data"]) == false) || ($Evento["Partecipanti"] == 0) || ($Evento["Approvato"] == false)): ?>
    <div class="card-footer text-center">
      <a class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark"
        data-toggle="modal" href="Modale.php" style="border-color: white;" data-target="#NonDisponibile">NON
        DISPONIBILE</a>
    </div>
    <?php endif; ?>
    <?php endif; ?>
  </div>
</div>

<!-- Modale NON DISPONIBILITA-->
<div style="font-size: 12pt; font-family: Tw Cen MT, verdana, sans-serif;" id="NonDisponibile" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">NON DISPONIBILE</h4>
            </div>
            <div class="modal-body">
                <p>Siamo spiacenti ma l'evento da lei selezionato non è al momento disponibile.</p>
            </div>
            <div class="modal-footer">
            <button type="button"  style="border-color: white;" class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 bg-dark" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>
<!-- Fine Modale -->


<?php endforeach; ?>
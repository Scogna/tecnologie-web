<?php

function isActive($pagename){
    if(basename($_SERVER['PHP_SELF'])==$pagename){
        echo " class='active' ";
    }
}

function getIdFromName($name){
    return preg_replace("/[^a-z]/", '', strtolower($name));
}

function isUserLoggedIn(){
    return !empty($_SESSION['PK_utente']);
}


function registerClient($user){
   $_SESSION["username"] = $user["username"];
   $_SESSION["nome"] = $user["Nome"];
   $_SESSION["PK_utente"] = $user["PK_utente"];
   $_SESSION["Email"] = $user["Email"];
   $_SESSION["Cognome"] = $user["Cognome"];
}

function registerOrg($user){
   if($user["username"] == "Admin"){
      $_SESSION["Admin"] = "true";

   }
   $_SESSION["username"] = $user["username"];
   $_SESSION["nome"] = $user["Nome"];
   $_SESSION["PK_organizzatore"] = $user["PK_organizzatore"];
   $_SESSION["Email"] = $user["Email"];
   $_SESSION["Cognome"] = $user["Cognome"];
}





function sec_session_start() {
        
        $session_name = 'sec_session_id'; // Imposta un nome di sessione
        $secure = false; // Imposta il parametro a true se vuoi usare il protocollo 'https'.
        $httponly = true; // Questo impedirà ad un javascript di essere in grado di accedere all'id di sessione.
        ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
        $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
        session_name($session_name); // Imposta il nome di sessione con quello prescelto all'inizio della funzione.
        session_start(); // Avvia la sessione php.
        session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
}


function uploadImage($path, $image){
  
   $imageName = basename($image["name"]);
   $fullPath = $path.$imageName;
   
   $maxKB = 5000000;
   $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
   $result = 0;
   $msg = "";
   //Controllo se immagine è veramente un'immagine
   $imageSize = getimagesize($image["tmp_name"]);
   if($imageSize === false) {
       $msg .= "File caricato non è un'immagine! ";
   }
   //Controllo dimensione dell'immagine < 500KB
   if ($image["size"] > $maxKB * 1024) {
       $msg .= "File caricato pesa troppo! Dimensione massima è $maxKB KB. ";
   }

   //Controllo estensione del file
   $imageFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
   if(!in_array($imageFileType, $acceptedExtensions)){
       $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
   }

   //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
   if (file_exists($fullPath)) {
       $i = 1;
       do{
           $i++;
           $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME)."_$i.".$imageFileType;
       }
       while(file_exists($path.$imageName));
       $fullPath = $path.$imageName;
   }

   //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
   if(strlen($msg)==0){
       if(!move_uploaded_file($image["tmp_name"], $fullPath)){
           $msg.= "Errore nel caricamento dell'immagine.";
       }
       else{
           $result = 1;
           $msg = $imageName;
       }
   }
   return array($result, $msg);
}

 function checkbrute($user_id, $mysqli) {
    // Recupero il timestamp
    $now = time();
    // Vengono analizzati tutti i tentativi di login a partire dalle ultime due ore.
    $valid_attempts = $now - (2 * 60 * 60); 
    if ($stmt = $mysqli->prepare("SELECT time FROM numerologin WHERE user_id = ? AND time > '$valid_attempts'")) { 
       $stmt->bind_param('i', $user_id); 
       // Eseguo la query creata.
       $stmt->execute();
       $stmt->store_result();
       // Verifico l'esistenza di più di 5 tentativi di login falliti.
       if($stmt->num_rows > 5) {
          return true;
       } else {
          return false;
       }
    }
 }
  
  
  function login($username, $password,$mysqli) {
    // Usando statement sql 'prepared' non sarà possibile attuare un attacco di tipo SQL injection.
    if ($stmt = $mysqli->prepare("SELECT PK_utente, Username, Password, Salt, Nome, Immagine, Email, Cognome FROM utente WHERE Username = ? LIMIT 1")) { 
       $stmt->bind_param('s', $username); // esegue il bind del parametro '$email'.
       $stmt->execute(); // esegue la query appena creata.
       $stmt->store_result();
       $stmt->bind_result($user_id, $username, $db_password, $salt, $nome, $img, $email, $cognome); // recupera il risultato della query e lo memorizza nelle relative variabili.
       $stmt->fetch();//recupera il ris
       $password = hash('sha512', $password.$salt); // codifica la password usando una chiave univoca.
       if($stmt->num_rows == 1) { // se l'utente esiste
          // verifichiamo che non sia disabilitato in seguito all'esecuzione di troppi tentativi di accesso errati.
          if(false) { 
             // Account disabilitato
             // Invia un e-mail all'utente avvisandolo che il suo account è stato disabilitato.
             return false;
          } else {
          if($db_password == $password) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
             // Password corretta!            
                $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente.
  
                $user_id = preg_replace("/[^0-9]+/", "", $user_id); // ci proteggiamo da un attacco XSS
                $_SESSION['PK_utente'] = $user_id; 
                $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); // ci proteggiamo da un attacco XSS
                $_SESSION['username'] = $username;
                $_SESSION['nome'] = $nome;
                $_SESSION['immagineUtente'] = $img;
                $_SESSION["Email"] = $email;
                $_SESSION["Cognome"] = $cognome;
                $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
                // Login eseguito con successo.
                return true;    
          } else {
             // Password incorretta.
             // Registriamo il tentativo fallito nel database.
             $now = time();
             $mysqli->query("INSERT INTO numerologin (user_id, time) VALUES ('$user_id', '$now')");
             return false;
          }
       }
       } else {
          // L'utente inserito non esiste.
          return false;
       }
    }
 }

 function loginOrg($username, $password,$mysqli) {
    // Usando statement sql 'prepared' non sarà possibile attuare un attacco di tipo SQL injection.
    if ($stmt = $mysqli->prepare("SELECT PK_organizzatore, Username, Password, Salt, Nome  FROM organizzatore WHERE Username = ? LIMIT 1")) { 
       $stmt->bind_param('s', $username); // esegue il bind del parametro '$email'.
       $stmt->execute(); // esegue la query appena creata.
       $stmt->store_result();
       $stmt->bind_result($user_id, $username, $db_password, $salt, $nome ); // recupera il risultato della query e lo memorizza nelle relative variabili.
       $stmt->fetch();//recupera il ris
       $password = hash('sha512', $password.$salt); // codifica la password usando una chiave univoca.
       if($stmt->num_rows == 1) { // se l'utente esiste
          // verifichiamo che non sia disabilitato in seguito all'esecuzione di troppi tentativi di accesso errati.
          if(false) { 
             // Account disabilitato
             // Invia un e-mail all'utente avvisandolo che il suo account è stato disabilitato.
             return false;
          } else {
          if($db_password == $password) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
             // Password corretta!            
                $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente.
  
                $user_id = preg_replace("/[^0-9]+/", "", $user_id); // ci proteggiamo da un attacco XSS
                $_SESSION['PK_organizzatore'] = $user_id; 
                $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); // ci proteggiamo da un attacco XSS
                $_SESSION['username'] = $username;
                $_SESSION['nome'] = $nome;
                $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
                // Login eseguito con successo.
                return true;    
          } else {
             // Password incorretta.
             // Registriamo il tentativo fallito nel database.
             $now = time();
             $mysqli->query("INSERT INTO numerologin (user_id, time) VALUES ('$user_id', '$now')");
             return false;
          }
       }
       } else {
          // L'utente inserito non esiste.
          return false;
       }
    }
 }



 function login_check($mysqli) {
    // Verifica che tutte le variabili di sessione siano impostate correttamente
    if(isset($_SESSION['PK_user'], $_SESSION['username'], $_SESSION['login_string'])) {
      $user_id = $_SESSION['PK_user'];
      $login_string = $_SESSION['login_string'];
      $username = $_SESSION['username'];     
      $user_browser = $_SERVER['HTTP_USER_AGENT']; // reperisce la stringa 'user-agent' dell'utente.
      if ($stmt = $mysqli->prepare("SELECT password FROM utente WHERE PK_utente = ? LIMIT 1")) { 
         $stmt->bind_param('i', $user_id); // esegue il bind del parametro '$user_id'.
         $stmt->execute(); // Esegue la query creata.
         $stmt->store_result();
  
         if($stmt->num_rows == 1) { // se l'utente esiste
            $stmt->bind_result($password); // recupera le variabili dal risultato ottenuto.
            $stmt->fetch();
            $login_check = hash('sha512', $password.$user_browser);
            if($login_check == $login_string) {
               // Login eseguito!!!!
               return true;
            } else {
               //  Login non eseguito
               return false;
            }
         } else {
             // Login non eseguito
             return false;
         }
      } else {
         // Login non eseguito
         return false;
      }
    } else {
      // Login non eseguito
      return false;
    }
 }

 function GetDay($data){
   $anno = substr($data,0,4);
   $mese = substr($data,5,2);
   $giorno = substr($data,8,2);
   $giornoInt = (int)$giorno;
   $meseInt = (int)$mese;
   $annoInt = (int)$anno;
   $giornoOdierno = (int)date("d");
   $meseOdierno = (int)date("m");
   $annoOdierno = (int)date("Y");
   $res = false;
   if($annoInt > $annoOdierno){
      $res = true;
   }
   if(($annoInt == $annoOdierno) && ($meseInt > $meseOdierno)){
      $res = true;
   }
   if(($annoInt == $annoOdierno) && ($meseInt == $meseOdierno) && ($giornoInt > $giornoOdierno)){
      $res = true;
   }
   if(($annoInt < $annoOdierno)){
      $res = false;
   }
   if(($annoInt == $annoOdierno) && ($meseInt < $meseOdierno)){
      $res = false;
   }
   if(($annoInt == $annoOdierno) && ($meseInt == $meseOdierno) && ($giornoInt < $giornoOdierno)){
      $res = false;
   }
    return $res;

 }

 function getMouth($data){
  
   $mese = substr($data,5,2);
  
   if(  $mese == "01"){
       $result = "Gennaio";
    }
    if( $mese == "02"){
      $result = "Febbraio";
   }
   if( $mese == "03"){
      $result = "Marzo";
   }
   if( $mese == "04"){
      $result = "Aprile";
   }
   if( $mese == "05"){
      $result = "Maggio";
   }
   if($mese == "06"){
      $result = "Giugno";
   }
   if($mese == "07"){
      $result = "Luglio";
   }
   if($mese == "08"){
      $result = "Agosto";
   }
   if($mese == "09"){
      $result = "Settembre";
   }
   if($mese == "10"){
      $result = "Ottobre";
   }
   if($mese == "11"){
      $result = "Novembre";
   }
   if($mese == "12"){
      $result = "Dicembre";
   }

   return $result;
 }

 function generaStringaRandom($lunghezza) {
   $caratteri = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $stringaRandom = '';
   for ($i = 0; $i < $lunghezza; $i++) {
       $stringaRandom .= $caratteri[rand(0, strlen($caratteri) - 1)];
   }
   return $stringaRandom;
}

?>
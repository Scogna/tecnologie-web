<?php
require_once 'connection.php';

if(isset($_GET["Approvato"]) && $_GET["Evento"]){
 $dbh->approva($_GET["Evento"]);
}

$templateParams["nav"] = "nav.php";
$templateParams["ClubCasuali"] = $dbh->getRandomClub(3); 
if(isset($_SESSION["Admin"])){
    $templateParams["Eventi"] = $dbh->getEvent();
}else{
    $templateParams["Eventi"] = $dbh->getRandomEvent(4); 
}
    


$templateParams["Evento"] = "Eventi\Event.php";
$templateParams["EventoAdmin"] = "Eventi\EventAdmin.php";
$templateParams["Clubs"] = "ClubHomePage.php";


if(isset($_POST["NomeClub"]) && isset($_POST["DescrizioneHomePage"]) && isset($_POST["DescrizioneDettagliata"]) && isset($_POST["FamosaPer"]) && isset($_POST["FesteFamose"]) && isset($_POST["ComeArrivare"]) && isset($_FILES["imgClub"])){
if(isset($_FILES["imgClub"])){
        list($result, $msg) =  uploadImage(UPLOAD_DIR, $_FILES["imgClub"]);
        if($result != 0){
            $img = $msg;
        }else{
            $img= "defaultEvent.jpg";
       }
    }
    $dbh->insertClub($_POST["NomeClub"],$_POST["DescrizioneHomePage"],$_POST["DescrizioneDettagliata"],$_POST["FamosaPer"],$_POST["FesteFamose"],$_POST["ComeArrivare"],$img);

}
require 'Template/HomePage.php';
?>